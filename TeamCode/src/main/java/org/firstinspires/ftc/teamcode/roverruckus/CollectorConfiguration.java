package org.firstinspires.ftc.teamcode.roverruckus;

import com.acmerobotics.dashboard.config.Config;

@Config
public class CollectorConfiguration {
    public static double COLLECTOR_RAISING_POSITION = 1;
    public static double COLLECTOR_LOWERING_POSITION = 0.24;
    public static double MINERAL_COLLECTION_POWER = 0.5;
}
