package org.firstinspires.ftc.teamcode.FTC2019;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

import java.util.List;

/**
 * Detects the gold mineral in sample fields and shows the result on the telemetry.
 */
@TeleOp(name = "Sample Detection", group = "Testing")
public final class SampleDetectionOpMode extends LinearOpMode {
    private final String VUFORIA_KEY = "AR0/zTT/////AAAAmdIL8I" +
            "+g30YLo3EYuz3yAVA07nKyaJEVgMhWll6Na0ZrAnVTbnCofSVje6SKFXqgLYoReTavM8NfIDaY9qhKBLrqLs" +
            "gJRkQBEAhigOVa02Tm8L46Z1ARtAVwJCuMsT22966JlshpbxVXra2Vns+juHYRAEh7uI0jguxlmn1NbgEGw4" +
            "HJJndCLntwnUMGHDuqS2r4PsBUBL+dVrhRfuOXfzG8A77/68LRSXiImK7UEAi2HR5FLTj/QyzPaV1bYbxIqL" +
            "JcUEuqkZ92jFMGyL1HbxW2SaEjjZsoqd2hTmMum3oO5ZiLD9oX7T11aeCCzLBieV4eKFUnydvgnAIrTZYmLk" +
            "DfZBHLa1Y1S7P2y8ZS1iCV";
    private final String MODEL_ASSET_NAME = "RoverRuckus.tflite";
    private final String GOLD_MINERAL_LABEL = "Gold Mineral";
    private final String SILVER_MINERAL_LABEL = "Silver Mineral";

    private final double LEFT_POSITION = 0.6;
    private final double RIGHT_POSITION = 1.0;
    private final double CENTER_POSITION = 0.8;
    private final double POSITION_STEP = 0.1;
    private final double SHAKING_RANGE = 0.1;
    private final long SHAKING_INTERVAL = 75L;

    private VuforiaLocalizer vuforiaLocalizer;
    private TFObjectDetector tfObjectDetector;
    private DcMotor latchingMotor;
    private Servo positionServo;
    private Telemetry.Item positionItem;

    /** Shakes a servo.
     *
     * @param servo the servo to be shaken
     * @param times times to shake
     */
    private void shakeServo(Servo servo, int times) {
        for (int i = 0; i < times; ++i) {
            if (servo.getPosition() > 0.0) {
                servo.setPosition(servo.getPosition() - SHAKING_RANGE);
                sleep(SHAKING_INTERVAL);
                servo.setPosition(servo.getPosition() + SHAKING_RANGE);
                sleep(SHAKING_INTERVAL);
            }

            if (servo.getPosition() < 1.0) {
                servo.setPosition(servo.getPosition() + SHAKING_RANGE);
                sleep(SHAKING_INTERVAL);
                servo.setPosition(servo.getPosition() - SHAKING_RANGE);
                sleep(SHAKING_INTERVAL);
            }
        }
    }

    private void detectAndShow() {
        // Assign position of the servo with predefined constants.
        if (gamepad1.a) {
            positionServo.setPosition(LEFT_POSITION);
        } else if (gamepad1.b) {
            positionServo.setPosition(RIGHT_POSITION);
        } else if (gamepad1.x) {
            positionServo.setPosition(CENTER_POSITION);
        }

        // Increment and decrement position of the servo with d-pad on the gamepad.
        if (gamepad1.dpad_left) {
            sleep(200);
            if (positionServo.getPosition() != 0.0) {
                positionServo.setPosition(positionServo.getPosition() - POSITION_STEP);
            }
        } else if (gamepad1.dpad_right) {
            sleep(200);
            if (positionServo.getPosition() != 0.0) {
                positionServo.setPosition(positionServo.getPosition() + POSITION_STEP);
            }
        }

        if (gamepad1.y) {
            sleep(200);
            shakeServo(positionServo, 2);
        }

        positionItem.setValue("%.2f", positionServo.getPosition());

        if (tfObjectDetector != null) {
            List<Recognition> recognitions = tfObjectDetector.getUpdatedRecognitions();
            if (recognitions != null) {
                telemetry.addData("Recognition Count", recognitions.size());
                for (int i = 0; i < recognitions.size(); ++i) {
                    if (recognitions.get(i).getLabel().equals(GOLD_MINERAL_LABEL)) {
                        telemetry.addData(String.format("R#%d", i + 1), "Gold");
                    } else {
                        telemetry.addData(String.format("R#%d", i + 1), "Silver");
                    }
                }
                telemetry.update();
            }
        }
    }

    private void initializeVuforiaLocalizer() {
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();
        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.FRONT;
        parameters.vuforiaLicenseKey = VUFORIA_KEY;

        vuforiaLocalizer = ClassFactory.getInstance().createVuforia(parameters);
    }

    private void initializeTFObjectDetector() {
        int monitorViewID = hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters parameters = new TFObjectDetector.Parameters(monitorViewID);

        tfObjectDetector = ClassFactory.getInstance().createTFObjectDetector(
                parameters, vuforiaLocalizer);
        tfObjectDetector.loadModelFromAsset(
                MODEL_ASSET_NAME, GOLD_MINERAL_LABEL, SILVER_MINERAL_LABEL);
    }

    @Override
    public final void runOpMode() throws InterruptedException {
        initializeVuforiaLocalizer();
        initializeTFObjectDetector();

        latchingMotor = hardwareMap.get(DcMotor.class, "g");
        positionServo = hardwareMap.get(Servo.class, "p");
        positionServo.setPosition(CENTER_POSITION);
        positionItem = telemetry.addData("Servo Position", "%.2f", positionServo.getPosition());
        positionItem.setRetained(true);

        telemetry.addData("Motor Zero Power Behavior", latchingMotor.getZeroPowerBehavior());
        telemetry.addData(">", "Press <START> to begin the Op Mode.");
        telemetry.update();

        waitForStart();
        if (tfObjectDetector != null) {
            tfObjectDetector.activate();
        }
        while (opModeIsActive()) {
            detectAndShow();
        }
        if (tfObjectDetector != null) {
            tfObjectDetector.deactivate();
        }
    }
}
