package org.firstinspires.ftc.teamcode.FTC2019;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

/**
 * Created by my on 2018/2/22.
 */
@TeleOp(name = "AutoOpLeft3")
public class AutoOpLeft3 extends AutoOpBase {
    @Override
    public void runOpMode() throws InterruptedException {
        initBot();
        waitForStart();

        driveLeftDistance(5, 0.3, 0.3, 0.7, 0.7);
        while (opModeIsActive()) {
            idle();
        }
    }
}
