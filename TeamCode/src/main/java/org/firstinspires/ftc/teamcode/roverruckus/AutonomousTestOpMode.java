package org.firstinspires.ftc.teamcode.roverruckus;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.BaseRobot;

import static org.firstinspires.ftc.teamcode.roverruckus.AutonomousConfiguration.AUTONOMOUS_CHASSIS_POWER;
import static org.firstinspires.ftc.teamcode.roverruckus.AutonomousConfiguration.COUNTS_PER_ROUNDS;
import static org.firstinspires.ftc.teamcode.roverruckus.InputConfiguration.KEY_PRESSING_DELAY;

@Disabled
@TeleOp(name = "Autonomous Test", group = "Testing")
public class AutonomousTestOpMode extends OpMode {
    private BaseRobot robot;

    private void waitForKeyReleased() {
        try {
            Thread.sleep(KEY_PRESSING_DELAY);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public final void init() {
        robot = new BaseRobot(hardwareMap);
    }

    @Override
    public final void start() {
        robot.reset();
    }

    @Override
    public final void loop() {
        if (gamepad1.dpad_up) {
            waitForKeyReleased();
            robot.driveForward(AUTONOMOUS_CHASSIS_POWER, (int) COUNTS_PER_ROUNDS);
        } else if (gamepad1.dpad_down) {
            waitForKeyReleased();
            robot.driveBackward(AUTONOMOUS_CHASSIS_POWER, (int) COUNTS_PER_ROUNDS);
        } else if (gamepad1.dpad_left) {
            waitForKeyReleased();
            robot.driveLeftSideWay(AUTONOMOUS_CHASSIS_POWER, (int) COUNTS_PER_ROUNDS);
        } else if (gamepad1.dpad_right) {
            waitForKeyReleased();
            robot.driveRightSideWay(AUTONOMOUS_CHASSIS_POWER, (int) COUNTS_PER_ROUNDS);
        }
    }

    @Override
    public final void stop() {
        robot.stopChassis();
    }
}
