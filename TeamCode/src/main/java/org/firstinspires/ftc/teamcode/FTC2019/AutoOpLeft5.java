package org.firstinspires.ftc.teamcode.FTC2019;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

/**
 * Created by my on 2018/2/22.
 */
@TeleOp(name = "AutoOpLeft5")
public class AutoOpLeft5 extends AutoOpBase {
    @Override
    public void runOpMode() throws InterruptedException {
        initBot();
        waitForStart();

        driveLeftDistance(5, 0.5, 0.5, 0.5, 0.5);
        while (opModeIsActive()) {
            idle();
        }
    }
}
