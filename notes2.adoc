= 笔记
:figure-caption: 图
:lang: zh-cmn-Hans-CN
:last-update-label: 最后更新于
:stem:
:source-highlighter: pygments

== 程序的组织

除了特别简单的程序，程序通常有一套组织方法。
通常使用的组织方法是自顶向下法，这种方法先编写程序最高级的部分，它由对较低级的过程的调用组成。
在编写好高级部分后，各个较低级的过程被逐个编写，最后形成能运行的程序。

另一种较不常用的是自底向上法，它与自顶向下法恰恰相反。
使用它编写程序时，先编写最低级的部分，然后逐层编写较高级的部分，最后形成整个程序。
这种方法要求程序员有较高的组织与规划能力。

即使是一个中小型程序，如果它没有有效的组织，是很可怕的。
举个例子，机器人程序中有许多地方都需要操控电机，如果不组织程序，每个要操控电机的地方都重新写一遍操控的代码。
如果你发现这些操控代码中有一处错误，那就意味着要找出所有用过这段代码的地方，然后依次修改，既麻烦，又可能改错。

现代的编程语言都提供各种组织代码的方法，譬如函数、过程、类与模块，使得我们可以将重复代码组织起来，之后只要调用组织好的代码就行了。
而且，如果在组织好的代码中发现错误，只需要修改一次就行了，不用修改所有使用它的地方。

在机器人的编程中，我们最常使用的组织方法是类和方法。
方法是对类中的函数与过程的称呼。

如下是 Java 中的类与方法：

[source,java]
--------------------------
// 定义一个类。
public class MyClass {
    // 在类中定义一个方法。
    public void myMethod() {
        // 在此编写方法的代码。
    }
}
--------------------------

如下是 Kotlin 中的类与方法：

[source,kotlin]
--------------------------
// 定义一个类。
class MyClass {
    // 在类中定义一个方法。
    fun myMethod() {
        // 在此编写方法的代码。
    }
}
--------------------------

== 常量

在谈论常量之前，让我们来看一个例子。
想象我们正在编写一个几何程序，其中有许多需要用到圆周率的地方。
在这里，我们将 3.14 作为圆周率使用。

[source,java]
--------------------------
public class Circle {
    public double radius;

    Circle(double r) {
        radius = r;
    }

    /** 获取直径。 */
    public double getDiameter() {
        return radius * 2;
    }

    /** 获取周长。 */
    public double getPerimeter() {
        return 2 * 3.14 * radius;
    }

    /** 获取面积。 */
    public double getArea() {
        return 3.14 * Math.pow(radius, 2.0);
    }
}

// 部分代码已省略。
--------------------------

可以看到，在这段代码中所有使用圆周率的地方都直接使用了 3.14。
但如果我们这时想增加圆周率的精度呢？
我们当然可以将代码中所有提到 3.14 的地方都替换为 3.1415926，但在一个真正的几何程序中，绝不能这样做。
因为其中可能有同样为 3.14，但并非圆周率的值。
手工寻找并修改每一个提到圆周率的地方当然也不现实。
幸运的是，现代的编程语言都为我们提供了一种称为常量的方法，来应对在多段代码中使用重复值的情形。

下面是将上段的示例代码改写成使用常量的结果：

[source,java]
--------------------------
public class Circle {
    public const double PI = 3.14;

    public double radius;

    Circle(double r) {
        radius = r;
    }

    /** 获取直径。 */
    public double getDiameter() {
        return radius * 2;
    }

    /** 获取周长。 */
    public double getPerimeter() {
        return 2 * PI * radius;
    }

    /** 获取面积。 */
    public double getArea() {
        return PI * Math.pow(radius, 2.0);
    }
}

// 部分代码已省略。
--------------------------

可以看到，在所有使用圆周率的地方，3.14 都被替换成了 `PI`。
这意味这我们只需要修改上面的 `PI = 3.14`，就可以一次更改程序中使用的所有圆周率。
不仅是数值，常量适用于任何类型的值。

下面的代码在 Java 中定义一个常量：

[source,java]
--------------------------
// 定义一个名为 FRUIT_NAME 的字符串常量。
const String FRUIT_NAME = "苹果";
--------------------------

下面的代码在 Kotlin 中定义一个常量：

[source,kotlin]
--------------------------
// 定义一个名为 FRUIT_NAME 的字符串常量。
val FRUIT_NAME: String = "苹果"
--------------------------

值得注意的是，Java 中的常量只能在类中定义，而 Kotlin 中的常量既可以在类中定义，也可以在方法中定义。

== 为什么矿石的识别失败了？

本年度的比赛引入了基于 Google 的机器学习库 TensorFlow 的物体识别系统，它用来识别金矿石与银矿石的位置。
更新的 FTC 库附带了一个 `ConceptTensorFlowObjectDetection` 程序，它展示了如何使用物体识别器来识别位置。
在启动这个示例后，机器人控制器的屏幕上会显示 TensorFlow 识别的结果，操控站的遥测部分也会显示出识别到的矿石数量与金矿石的位置。

.在使用示例时机器人控制器的屏幕
image::tfod-rc.png[]

.在使用示例时操控站的屏幕
image::tfod-ds.png[]

在第一次使用 TensorFlow 识别矿石时，我们发现它的识别结果是飘忽不定的。
也就是说，物体识别器在检测金矿石的位置时是飘忽不定的。
比如说，在金矿石位于最右边的情况下，识别器有时显示它在中间，有时又显示它在左边。

.识别错误时机器人控制器的屏幕。此处，金矿石位于右侧。
image::tfod-error-rc.png[]

.识别错误时操控站的遥测界面。此处，金矿石的位置被错误地识别成左侧。
image::tfod-error-ds-1.png[]

.识别错误时操控站的遥测界面。此处，金矿石的位置被错误地识别成中间。
image::tfod-error-ds-2.png[]

在官方的识别示例中，识别是连续进行的。
除非你按下操控站上的“停止”按钮，否则识别不会停止。
在机器人的自动程序中，我们不需要连续识别，而是只需要识别一次，并获取结果。

要达到这一点，最简单的方法就是去除示例中的循环。
在官方示例中，整个识别部分被包裹在循环中，如下所示：

[source,java]
--------------------------
while (opModeIsActive()) {
    if (tfod != null) {
        List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
        if (updatedRecognitions != null) {
            // 部分代码已省略。
        }
    }
}
--------------------------

当去除了循环的包裹，里面的识别程序就会只执行一次。
这样，识别只执行一次，之后就返回识别结果，与需求一模一样。
但在尝试了这种方法后，我们发现它的识别结果通常是错误的。

这时，我们猜测可能是在 Op Mode 立即启动后就识别矿石会导致识别错误，所以我们在识别的代码之前加入了 3 秒延时。
这次尝试同样也失败了。

最后，我们认为这是识别器固有的问题，所以想出了一种通过多次识别来增大识别精度的方法。
这种方法的本质是收集每次识别的结果并比较它们，最后得出识别结果。

在这种方法中，我们首先在程序中定义了一个用来存放识别结果的数组：

[source,java]
--------------------------
int positionCounts[] = {0, 0, 0};
--------------------------

上面的数组有三个元素。
第一个元素是金矿石在左边的情况的计数，第二个是金矿石在中间的情况的计数，第三个是金矿石在右边的情况的计数。

在识别部分，我们使用的代码与官方示例一致。
但在判断矿石位置的代码部分，我们在每种情况的判断的代码最后都增加了用来递增上面的数组的代码。

[source,java]
--------------------------
if (goldMineralX < silverMineral1X && goldMineralX < silverMineral2X) {
    telemetry.addData("Gold Mineral Position", "Left");
    ++positionCounts[0];
} else if (goldMineralX > silverMineral1X && goldMineralX > silverMineral2X) {
    telemetry.addData("Gold Mineral Position", "Right");
    ++positionCounts[1];
} else {
    telemetry.addData("Gold Mineral Position", "Center");
    ++positionCounts[2];
}
--------------------------

在循环的开头，我们增加了一个计数器来限制程序重复执行的次数：

[source,java]
--------------------------
int rounds = 0;
--------------------------

而在循环的末尾，也增加了一个判断来决定何时结束循环：

[source,java]
--------------------------
++rounds;
if (rounds >= 100) {
    break;
}
--------------------------

这里，100 指的是限制循环执行 100 遍。

但即使使用了这个方法，矿石识别的正确率仍然相当差劲。
下面是对识别成功率的统计：

|=========================
|方法 |左侧成功率 |中间成功率 |右侧成功率
|直接获取 |stem:[1/5] |stem:[1/10] |stem:[1/10]
|延时 3 秒后获取 |stem:[1/10] |stem:[3/10] |stem:[1/5]
|循环 100 次，统计后获取 |stem:[3/10] |stem:[1/5] |stem:[3/10]
|=========================

FTC 论坛上有一篇名为《Phone Orientation for TensorFlow》的帖子，其中模糊地指出了可以按照屏幕旋转的方向更换本来用于获得被识别物体位置的 `getRight()` 方法。
我们尝试性地将对此方法的调用更换为对 `getLeft()` 方法的调用，但仍旧一无所获。

正当我们为此事一筹莫展，甚至认为这个问题永远也不可能解决，而准备使用颜色传感器作为替代时，我们的程序员偶然查看了 `TFObjectDetector` 接口的唯一实现 `TFObjectDetectorImpl` 类，发现了与屏幕旋转有关的代码：

[source,java]
--------------------------
private static int getRotation(Activity activity, CameraName cameraName) {
    int rotation = 0;

    if (cameraName instanceof BuiltinCameraName) {
        int displayRotation = 0;
        switch (activity.getWindowManager().getDefaultDisplay().getRotation()) {
            case Surface.ROTATION_0: displayRotation = 0; break;
            case Surface.ROTATION_90: displayRotation = 90; break;
            case Surface.ROTATION_180: displayRotation = 180; break;
            case Surface.ROTATION_270: displayRotation = 270; break;
        }

        // 部分代码已省略。
    }
}
--------------------------

`getRotation()` 方法的实现中，明确指出了物体识别器是通过从 Android 系统获取当前屏幕的旋转角度来自动调整的。
而 `Recognition` 接口的实现 `RecognitionImpl` 类的构造器更显示出物体识别器自动按旋转调整的特性：

[source,java]
--------------------------
RecognitionImpl(@NonNull CameraInformation cameraInformation,
        @NonNull String label, float confidence, @NonNull RectF location) {
    this.cameraInformation = cameraInformation;
    this.label = label;
    this.confidence = confidence;
    this.location = location;

    updatedLocation = new RectF(location);
    switch (cameraInformation.rotation) {
        default:
            throw new IllegalArgumentException("CameraInformation.rotation must be 0, 90, 180, or 2700.");
        case 0:
            frameHorizontalFocalLength = cameraInformation.horizontalFocalLength;
            frameWidth = cameraInformation.size.width;
            frameHeight = cameraInformation.size.height;
            break;
        case 90:
            frameHorizontalFocalLength = cameraInformation.verticalFocalLength;
            frameWidth = cameraInformation.size.height;
            frameHeight = cameraInformation.size.width;
            updatedLocation.left = location.top;
            updatedLocation.right = location.bottom;
            updatedLocation.top = cameraInformation.size.width - location.right;
            updatedLocation.bottom = cameraInformation.size.width - location.left;
            break;
        case 180:
            // 部分代码已省略。
    }
}
--------------------------

至此，我们才发现，原来困扰我们多少天的问题，竟然本身就不存在。
我们查看了机器人控制器手机的通知栏，发现“方向锁定”开关是开启的。
“方向锁定”阻止了手机屏幕的自动旋转，而物体识别器通过获取屏幕的旋转角度来判断手机的旋转角度。

在问题解决后，我们发现论坛上早有帖子指出了这个问题。
比如，《TensorFlow Lite Tutorial》的第 9 个回复，就提到了要使用 Blocks 中的 TensorFlow 示例 Op Mode，必须要将手机设置成自动旋转。
而这份回复也指出 FTC wiki 上的《Blocks Sample TensorFlow Object Detection Op Mode》早已对此作出说明。
这实在地证明了我们使用搜索引擎尚不熟练，仍要多加练习搜集信息的技巧。

== FTC 仪表板的使用

在上个赛季的比赛中，我们发现，在调试机器人的程序时，即使要作一些微调，也总要重新构建并在机器人控制器中安装程序。
而且，FTC 虽然为我们提供了在操控站上以遥测形式汇报程序运行状态的可能性，但操控站的屏幕太小，不利于查看。
本赛季，我们在程序中加入了由来自加利福尼亚州草谷的 ACME Robotics 队伍（编号 8367）编写的“FTC 仪表板”。
FTC 仪表板在机器人控制器上提供了可以在电脑上连接并查看的界面，使遥测的查看变得更方便。
不仅如此，它还允许我们可以随时更改程序中的一些值，而不必重新构建程序。

为了在我们的程序中也使用 FTC 仪表板，我们首先要在 `ftc_app` 项目的根文件夹下的 `build.gradle` 构建脚本中加入对 JCenter 包仓库的引用。
`build.gradle` 是 `ftc_app` 项目的主构建脚本。
JCenter 是世界上最大的 Maven 包仓库，也是 Android 的 Gradle 插件使用的默认包仓库。
FTC 仪表板就保存在这个仓库上。

[source,groovy]
--------------------------
buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:3.1.3'
    }
}
allprojects {
    repositories {
        google()
        jcenter()
    }
}
--------------------------

上面的是未经更改的 `build.gradle` 文件。
我们并不需要手动在 `build.gradle` 文件中加入对 JCenter 包仓库的引用，因为它已成为 Android 的 Gradle 插件的默认包仓库。

下一步是在 `FtcRobotController` 项目的 `build.release.gradle` 构建脚本中增添对 FTC 仪表板的依赖。

[source,groovy]
--------------------------
dependencies {
    implementation (name:'Inspection-release', ext: 'aar')
    implementation (name:'Blocks-release', ext: 'aar')
    implementation (name:'RobotCore-release', ext: 'aar')
    implementation (name:'Hardware-release', ext: 'aar')
    implementation (name:'FtcCommon-release', ext: 'aar')
    implementation (name:'WirelessP2p-release', ext:'aar')
    implementation 'com.acmerobotics.dashboard:dashboard:0.2.1'
}
--------------------------

上面的文件的倒数第二行就是对 FTC 仪表板的依赖。
其中，`implementation` 指定这个依赖既在构建时需要，也在运行时需要。
在它后面，`com.acmerobotics.dashboard:dashboard:0.2.1` 标识符指定了程序要依赖的具体模块。
这个标识符被冒号分成三段，第一段指定了组名，第二段指定了模块名，第三段指定了版本号。
在这里，组名是 `com.acmerobotics.dashboard`，模块名是 `dashboard`，版本号是 `0.2.1`。
与 Java 的包名一样，组名也通常以反转的域名开头。
比如说，你的域名是 acmerobotics.com，那么组名或 Java 包名就会以 `com.acmerobotics` 开头，之后跟着项目的名称。

下一步是在 `TeamCode` 项目的 `build.release.gradle` 构建脚本中增加对 FTC 仪表板的依赖。

[source,groovy]
--------------------------
dependencies {
    implementation project(':FtcRobotController')
    implementation (name: 'RobotCore-release', ext: 'aar')
    implementation (name: 'Hardware-release', ext: 'aar')
    implementation (name: 'FtcCommon-release', ext: 'aar')
    implementation (name: 'WirelessP2p-release', ext:'aar')
    implementation (name: 'tfod-release', ext:'aar')
    implementation (name: 'tensorflow-lite-0.0.0-nightly', ext:'aar')
    implementation 'com.acmerobotics.dashboard:dashboard:0.2.1'
}
--------------------------

与 `FtcRobotController` 项目的 `build.release.gradle` 一样，上面的倒数第二行是对 FTC 仪表板的依赖。

下一步，在机器人控制器的主活动 `FtcRobotControllerActivity` 中增加启动 FTC 仪表板的代码。

[source,java]
--------------------------
@Override
protected void onCreate(Bundle savedInstanceState) {
    // 部分代码已省略。
    logPackageVersions();
    logDeviceSerialNumber();
    RobotLog.logDeviceInfo();
    if (preferencesHelper.readBoolean(
            getString(R.string.pref_wifi_automute), false)) {
        initWifiMute(true);
    }
    FtcDashboard.start();
}
--------------------------

上面是 `onCreate()` 方法，在最后增加了一行启动 FTC 仪表板的代码。

[source,java]
--------------------------
@Override
protected void onDestroy() {
    // 部分代码已省略。
    ServiceController.stopService(
        FtcRobotControllerWatchdogService.class);
    wifiLock.release();
    preferencesHelper.getSharedPreferences()
        .unregisterOnSharedPreferenceChangeListener(
            sharedPreferencesListener);
    RobotLog.cancelWriteLogcatToDisk();
    FtcDashboard.stop();
}
--------------------------

上面是 `onDestroy()` 方法，在最后增加了一行终止 FTC 仪表板的代码。

下一步是在主活动的服务绑定方法中增加将 FTC 仪表板连接到机器人控制器内置的服务器的代码。
机器人控制器内置的服务器为队伍提供了除 Android Studio 以外的两种编程方法 Blocks 和 OnBot Java。
但我们也可以将其他的服务，比如文中所述的 FTC 仪表板连接到其上。

[source,java]
--------------------------
public void onServiceBind(
        final FtcRobotControllerService service) {
    // 部分代码已省略。
    programmingWebHandlers.setState(
        new FtcRobotControllerServiceState() {
            @NonNull
            @Override
            public WebServer getWebServer() {
                return service.getWebServer();
            }

            @Override
            public EventLoopManager getEventLoopManager() {
                return service.getRobot().eventLoopManager;
            }
        }
    );
    FtcDashboard.attachWebServer(service.getWebServer());
}
--------------------------

上面是 `onServiceBind()` 方法，在最后增加了一行将 FTC 仪表板连接到内置服务器的代码。

最后一步是添加代码以使 FTC 仪表板的 Op Mode 管理功能工作。
FTC 仪表板提供了简单的 Op Mode 管理功能，包括选择、初始化、启动与停止 Op Mode。
为了能管理 Op Mode，FTC 仪表板要连接到事件循环上。

[source,java]
--------------------------
private void requestRobotSetup(
        @Nullable Runnable runOnComplete) {
    // 部分代码已省略。
    FtcEventLoopIdle idleLoop = new FtcEventLoopIdle(
        hardwareFactory,
        userOpModeRegister,
        callback,
        this,
        programmingModeController
    );
    controllerService.setCallback(callback);
    controllerService.setupRobot(
        eventLoop, idleLoop, runOnComplete);
    passReceivedUsbAttachmentsToEventLoop();
    FtcDashboard.attachEventLoop(eventLoop);
}
--------------------------

上面是 `requestRobotSetup()` 方法，在最后增加了一行将 FTC 仪表板连接到事件循环的代码。

重新构建并运行机器人控制器后，在菜单中选择“Program & Manage”项，控制器会显示出无线网名称与密码。
将电脑连接到这个网络后，使用浏览器打开 http://192.168.49.1:8080/dash 网址，就会进入 FTC 仪表板的界面。

.机器人控制器的菜单。红色矩形标记出“Program & Manage”项。
image::program-manage-menu.png[]

.点选“Program & Manage”后出现的“机器人控制器连接信息”界面。红色下划线标记出无线网名称与密码。
image::program-manage.png[]

.选择机器人控制器的无线网络
image::wifi-select.png[]

.FTC 仪表板的界面
image::ftc-dashboard.png[]

在界面中，可以看到五个部分：Op Mode、Field、Graph、Configuration 和 Telemetry。
Op Mode 部分可以控制 Op Mode 的运行。
Field 部分允许程序在上面绘图以展示比赛场地的状况。
Graph 部分可以用一些遥测的值绘制图表。
Configuration 部分可以即时修改一些预先规定的值。
Telemetry 部分与操控站的遥测功能一样，可以显示一些程序汇报的信息。

为了使用 Configuration 部分，需要至少有一个带 `@Config` 注解的类。
`@Config` 注解让 FTC 仪表板了解某个类要用在 Configuration 部分中，如同用来注册 Op Mode 的 `@TeleOp` 与 `@Autonomous` 注解一样。
这个类应该是公有的，且里面的属性都是公有、静态、可改变的。

下面是 Java 中使用 `@Config` 注解的类的例子：

[source,java]
--------------------------
@Config
public class RobotConfiguration {
    public static double POWER_FACTOR = 2.0;
    public static boolean USE_TELEMETRY_PACKET = true;
}
--------------------------

下面是 Kotlin 中使用 `@Config` 注解的类的例子：

[source,kotlin]
--------------------------
@Config
object RobotConfiguration {
    @JvmField var POWER_FACTOR = 2.0
    @JvmField var USE_TELEMETRY_PACKET = true
}
--------------------------

FTC 仪表板的 Telementry 部分与操控站的遥测功能使用起来完全不同。
Telementry 部分使用 `TelementryPacket` 类，而遥测功能使用 `Telementry` 类。

下面的代码展示了 `TelementryPacket` 类的简单使用：

[source,java]
--------------------------
FtcDashboard dashboard = FtcDashboard.getInstance();
TelementryPacket packet = new TelementryPacket();

packet.put("a", 3.14);
packet.put("b", -5.1);
dashboard.sendTelementryPacket(packet);
--------------------------

可以看出，要使用 `TelementryPacket` 类，首先要获取当前 FTC 仪表板的实例，然后构造一个新的 `TelementryPacket` 实例。
使用 `put()` 方法可以添加项目，如果添加的是数值，则可以使用 Graph 部分绘制图表。
添加好所有项目后，用 `sendTelementryPacket()` 方法向仪表板发送遥测信息。

需要注意的是，`sendTelementryPacket()` 与 `telementry.update()` 方法不是一回事。
`telementry.update()` 方法在发送遥测时也会清除所有值，而 `sendTelementryPacket()` 会保留值。
因此，若要达到发送后清除的效果，只能再次构造一个全新的 `TelementryPacket` 实例。

[source,java]
--------------------------
packet = new TelementryPacket();
--------------------------

== 手动程序的设计

机器人在手动模式中的程序能根据手柄的按键来让车作出各种行为。

程序的第一部分是八向行走，由一号手柄的左摇杆控制。
摇杆不像十字键，它输出的是有范围的模拟值，而不是明确的“开‐关”信号。
为了让摇杆与八向行走一同工作，我通过为摇杆设定一个阈值来判断操控员作出的方向。
比如说，如果摇杆向左边和上边偏移的值都大于阈值，程序就判断操控员要向左上方行走。

[source,java]
--------------------------
public static double JOYSTICK_THRESHOLD = 0.2;
--------------------------

上面的代码定义了 `JOYSTICK_THRESHOLD` 常量，它就是上面所说的摇杆阈值。
这个常量定义在 FTC 仪表板的配置类中，允许我们在测试机器人时方便地更改它。

[source,kotlin]
--------------------------
val power = hypot(gamepad1.left_stick_x, gamepad1.left_stick_y).toDouble()
--------------------------

上面的代码根据摇杆动作的幅度计算出功率。

[source,kotlin]
--------------------------
if (gamepad1.left_stick_x > JOYSTICK_THRESHOLD
        && gamepad1.left_stick_y < -JOYSTICK_THRESHOLD) {
    packet.put("Direction", "NE")
    robot.driveNE(power)
} else if (gamepad1.left_stick_x > JOYSTICK_THRESHOLD
        && gamepad1.left_stick_y > JOYSTICK_THRESHOLD) {
    packet.put("Direction", "SE")
    robot.driveSE(power)
} else if (gamepad1.left_stick_x < -JOYSTICK_THRESHOLD
        && gamepad1.left_stick_y > JOYSTICK_THRESHOLD) {
    packet.put("Direction", "SW")
    robot.driveSW(power)
} else if (gamepad1.left_stick_x < -JOYSTICK_THRESHOLD
        && gamepad1.left_stick_y < -JOYSTICK_THRESHOLD) {
    packet.put("Direction", "NW")
    robot.driveNW(power)
} else if (gamepad1.left_stick_x > JOYSTICK_THRESHOLD) {
    // 部分代码已省略。
} else {
    packet.put("Direction", "Stop")
    robot.stop()
}
--------------------------

上面的代码展示了如何使用摇杆阈值判断当前使用的是哪个方向。

在此，不使用十字键而是摇杆的主要原因是操控摇杆比操控十字键更加舒适。
如果没有摇杆而只有十字键，我们当然只能使用十字键了。
幸运的是，Xbox 360 手柄不仅拥有摇杆，而且有两个！

相对传统的、自由方向的操控方式来说，八向行走有一些显著的优点：

- 八向行走的实现更清晰、易懂。
+
--
传统的操控方式虽然看起来更简单，但它在遇到问题时更难解决，很容易陷入“散弹枪编程”的困境。
左改一下、右改一下，有时可能碰运气搞好，但实际上阻碍了对程序的清晰理解。

[source,java]
----------------------
robot.driveWithMecanum(
    (double) Math.hypot(gamepad1.left_stick_x, gamepad1.left_stick_y),
    Math.atan2(gamepad1.left_stick_y, gamepad1.left_stick_x)  - Math.PI / 4,
    (double) gamepad1.right_stick_x
);
----------------------

上面的代码展示了传统操控模式的写法。

八向行走将可能的情况分为八种，很容易就能对程序进行分层，使程序更加易懂。
--
- 八向行走利于操控员掌握方向。
+
在比赛时，场地情况错综复杂，操控员很容易就误判了方向。
在使用传统操控方时，操控员按照自己判断的方向，用摇杆使机器人直接行走到目的地。
但若操作员误判了方向，机器人很容易就偏离目的地。
八向行走将机器人可能的方向固定成八种可能性，使得它行走的方向总是与想法一致，不会出现偏离的情况。

程序的第二部分是旋转控制，它以一号手柄的右摇杆控制机器人原地旋转。
旋转控制可以充分利用摇杆输出模拟值这一特性：
向左推摇杆，机器人就向左旋转；
向右推摇杆，机器人就向右旋转；
推摇杆的力度越大，机器人旋转的速度就越快。
这样的操作符合直觉。

[source,kotlin]
--------------------------
packet.put("Rotation", gamepad1.right_stick_x)
if (abs(gamepad1.right_stick_x) > JOYSTICK_THRESHOLD) {
    robot.driveWithMecanum(
            abs(gamepad1.right_stick_x).toDouble(),
            0.0,
            gamepad1.right_stick_x.toDouble()
    )
}
--------------------------

上面的代码展示了使用右摇杆控制转向的代码。
值得注意的是，在传统操控方式的实现代码中，行走方向与原地旋转的控制是整合在一起的。
而在八向行走的实现代码中，行走方向与原地旋转的控制是分离且互不相关的。
