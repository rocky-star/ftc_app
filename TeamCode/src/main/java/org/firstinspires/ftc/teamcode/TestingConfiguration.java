package org.firstinspires.ftc.teamcode;

import com.acmerobotics.dashboard.config.Config;

@Config
public class TestingConfiguration {
    public static long KEY_PRESSING_DELAY = 250;
    public static double SERVO_POSITION_STEP = 0.1;
    public static double SMOOTH_SERVO_POSITION_STEP = 0.05;
    public static long SMOOTH_SERVO_DELAY = 100;
}
