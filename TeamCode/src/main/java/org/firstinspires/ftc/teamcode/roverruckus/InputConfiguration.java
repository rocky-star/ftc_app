package org.firstinspires.ftc.teamcode.roverruckus;

import com.acmerobotics.dashboard.config.Config;

@Config
public class InputConfiguration {
    public static double JOYSTICK_THRESHOLD = 0.2;
    public static int KEY_PRESSING_DELAY = 500;
}
