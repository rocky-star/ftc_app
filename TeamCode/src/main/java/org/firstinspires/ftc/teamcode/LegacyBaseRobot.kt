package org.firstinspires.ftc.teamcode

import com.qualcomm.robotcore.hardware.DcMotor
import com.qualcomm.robotcore.hardware.DcMotorSimple
import com.qualcomm.robotcore.hardware.HardwareMap
import kotlin.math.cos
import kotlin.math.sin

import org.firstinspires.ftc.teamcode.roverruckus.LegacyConfiguration.*

abstract class LegacyBaseRobot(hwMap: HardwareMap) {
    val VUFORIA_KEY = ("AR0/zTT/////AAAAmdIL8I+g30YLo3EYuz3yAVA07nKyaJEVgMhWll6Na0ZrAnVTbnCofSVje6"
                       + "SKFXqgLYoReTavM8NfIDaY9qhKBLrqLsgJRkQBEAhigOVa02Tm8L46Z1ARtAVwJCuMsT2296"
                       + "6JlshpbxVXra2Vns+juHYRAEh7uI0jguxlmn1NbgEGw4HJJndCLntwnUMGHDuqS2r4PsBUBL"
                       + "+dVrhRfuOXfzG8A77/68LRSXiImK7UEAi2HR5FLTj/QyzPaV1bYbxIqLJcUEuqkZ92jFMGyL"
                       + "1HbxW2SaEjjZsoqd2hTmMum3oO5ZiLD9oX7T11aeCCzLBieV4eKFUnydvgnAIrTZYmLkDfZB"
                       + "HLa1Y1S7P2y8ZS1iCV")

    val hardwareMap = hwMap
    public val leftFrontMotor = hwMap.get(DcMotor::class.java, "motorLF")
    public val rightFrontMotor = hwMap.get(DcMotor::class.java, "motorRF")
    public val leftRearMotor = hwMap.get(DcMotor::class.java, "motorLR")
    public val rightRearMotor = hwMap.get(DcMotor::class.java, "motorRR")

    fun reset() {
        setDirection(DcMotorSimple.Direction.FORWARD)

        leftFrontMotor.mode = DcMotor.RunMode.STOP_AND_RESET_ENCODER
        rightFrontMotor.mode = DcMotor.RunMode.STOP_AND_RESET_ENCODER
        leftRearMotor.mode = DcMotor.RunMode.STOP_AND_RESET_ENCODER
        rightRearMotor.mode = DcMotor.RunMode.STOP_AND_RESET_ENCODER

        leftFrontMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        rightFrontMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        leftRearMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        rightRearMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER

        leftFrontMotor.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.BRAKE
        rightFrontMotor.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.BRAKE
        leftRearMotor.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.BRAKE
        rightRearMotor.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.BRAKE
    }

    fun setDirection(direction: DcMotorSimple.Direction) {
        setDirection(direction, direction, direction, direction)
    }

    fun setDirection(
            leftFront: DcMotorSimple.Direction,
            rightFront: DcMotorSimple.Direction,
            leftBack: DcMotorSimple.Direction,
            rightBack: DcMotorSimple.Direction) {
        leftFrontMotor.direction = leftFront
        rightFrontMotor.direction = rightFront.inverted()
        leftRearMotor.direction = leftBack
        rightRearMotor.direction = rightBack.inverted()
    }

    fun setPower(power: Double) {
        setPower(power, power, power, power)
    }

    fun setPower(leftFront: Double, rightFront: Double, leftRear: Double, rightRear: Double) {
        leftFrontMotor.power = leftFront
        rightFrontMotor.power = rightFront
        leftRearMotor.power = leftRear
        rightRearMotor.power = rightRear
    }

    fun driveWithMecanum(power: Double, direction: Double, rotation: Double) {
        setDirection(DcMotorSimple.Direction.FORWARD)

        leftFrontMotor.power = power * LEFT_FRONT_FACTOR * cos(direction) + rotation
        rightFrontMotor.power = power * RIGHT_FRONT_FACTOR * sin(direction) - rotation
        leftRearMotor.power = power * LEFT_REAR_FACTOR * sin(direction) + rotation
        rightRearMotor.power = power * RIGHT_REAR_FACTOR * cos(direction) - rotation
    }

    fun driveN(power: Double) {
        driveWithMecanum(power, 0.7853981633974483, 0.0)
    }

    fun driveNE(power: Double) {
        driveWithMecanum(power, 0.0, 0.0)
    }

    fun driveE(power: Double) {
        driveWithMecanum(power, -0.7853981633974483, 0.0)
    }

    fun driveSE(power: Double) {
        driveWithMecanum(power, -1.5707963267948966, 0.0)
    }

    fun driveS(power: Double) {
        driveWithMecanum(power, -2.356194490192345, 0.0)
    }

    fun driveSW(power: Double) {
        driveWithMecanum(power, -3.141592653589793, 0.0)
    }

    fun driveW(power: Double) {
        driveWithMecanum(power, 2.356194490192345, 0.0)
    }

    fun driveNW(power: Double) {
        driveWithMecanum(power, 1.5707963267948966, 0.0)
    }

    fun driveStraightAhead(power: Double, counts: Int) {
        leftFrontMotor.targetPosition = leftFrontMotor.currentPosition + counts
        rightFrontMotor.targetPosition = rightFrontMotor.currentPosition + counts
        leftRearMotor.targetPosition = leftRearMotor.currentPosition + counts
        rightRearMotor.targetPosition = rightRearMotor.currentPosition + counts
        leftFrontMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        rightFrontMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        leftRearMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        rightRearMotor.mode = DcMotor.RunMode.RUN_TO_POSITION

        setDirection(DcMotorSimple.Direction.FORWARD)
        setPower(power)

        while (leftFrontMotor.isBusy() || rightFrontMotor.isBusy() || leftRearMotor.isBusy() || rightRearMotor.isBusy()) {
        }

        leftFrontMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        rightFrontMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        leftRearMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        rightRearMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        stop()
    }

    fun driveSideWay(power: Double, counts: Int) {
        leftFrontMotor.targetPosition = leftFrontMotor.currentPosition + counts
        rightFrontMotor.targetPosition = rightFrontMotor.currentPosition + counts
        leftRearMotor.targetPosition = leftRearMotor.currentPosition + counts
        rightRearMotor.targetPosition = rightRearMotor.currentPosition + counts
        leftFrontMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        rightFrontMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        leftRearMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        rightRearMotor.mode = DcMotor.RunMode.RUN_TO_POSITION

        setDirection(
                DcMotorSimple.Direction.FORWARD,
                DcMotorSimple.Direction.REVERSE,
                DcMotorSimple.Direction.REVERSE,
                DcMotorSimple.Direction.FORWARD
        )
        setPower(
                power * LEFT_FRONT_FACTOR,
                power * RIGHT_FRONT_FACTOR,
                power * LEFT_REAR_FACTOR,
                power * RIGHT_REAR_FACTOR
        )

        while (leftFrontMotor.isBusy() || rightFrontMotor.isBusy() || leftRearMotor.isBusy() || rightRearMotor.isBusy()) {
        }

        leftFrontMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        rightFrontMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        leftRearMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        rightRearMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        stop()
    }

    fun driveDiagonal(power: Double, counts: Int) {
        leftFrontMotor.targetPosition = leftFrontMotor.currentPosition + counts
        rightRearMotor.targetPosition = rightRearMotor.currentPosition + counts
        leftFrontMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        rightRearMotor.mode = DcMotor.RunMode.RUN_TO_POSITION

        setDirection(DcMotorSimple.Direction.FORWARD)
        setPower(power, 0.0, 0.0, power)

        while (leftFrontMotor.isBusy() || rightRearMotor.isBusy()) {
        }

        leftFrontMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        rightRearMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        stop()
    }

    fun driveConcerning(power: Double, counts: Int) {
        leftFrontMotor.targetPosition = leftFrontMotor.currentPosition + counts
        leftRearMotor.targetPosition = leftRearMotor.currentPosition + counts
        leftFrontMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        leftRearMotor.mode = DcMotor.RunMode.RUN_TO_POSITION

        setDirection(DcMotorSimple.Direction.FORWARD)
        setPower(power, 0.0, power, 0.0)

        while (leftFrontMotor.isBusy() || leftRearMotor.isBusy()) {
        }

        leftFrontMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        leftRearMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        stop()
    }

    fun turnRound(power: Double, counts: Int) {
        leftFrontMotor.targetPosition = leftFrontMotor.currentPosition + counts
        rightFrontMotor.targetPosition = rightFrontMotor.currentPosition + counts
        leftRearMotor.targetPosition = leftRearMotor.currentPosition + counts
        rightRearMotor.targetPosition = rightRearMotor.currentPosition + counts
        leftFrontMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        rightFrontMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        leftRearMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        rightRearMotor.mode = DcMotor.RunMode.RUN_TO_POSITION

        setDirection(
                DcMotorSimple.Direction.FORWARD,
                DcMotorSimple.Direction.REVERSE,
                DcMotorSimple.Direction.FORWARD,
                DcMotorSimple.Direction.REVERSE
        )
        setPower(power)

        while (leftFrontMotor.isBusy() || rightFrontMotor.isBusy() || leftRearMotor.isBusy() || rightRearMotor.isBusy()) {
        }

        leftFrontMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        rightFrontMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        leftRearMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        rightRearMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        stop()
    }

    fun turnOfRearAxis(power: Double, counts: Int) {
        leftFrontMotor.targetPosition = leftFrontMotor.currentPosition + counts
        rightFrontMotor.targetPosition = rightFrontMotor.currentPosition + counts
        leftRearMotor.targetPosition = leftRearMotor.currentPosition + counts
        rightRearMotor.targetPosition = rightRearMotor.currentPosition + counts
        leftFrontMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        rightFrontMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        leftRearMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        rightRearMotor.mode = DcMotor.RunMode.RUN_TO_POSITION

        setDirection(
                DcMotorSimple.Direction.FORWARD,
                DcMotorSimple.Direction.REVERSE,
                DcMotorSimple.Direction.FORWARD,
                DcMotorSimple.Direction.REVERSE
        )
        setPower(power, power, 0.0, 0.0)

        while (leftFrontMotor.isBusy() || rightFrontMotor.isBusy() || leftRearMotor.isBusy() || rightRearMotor.isBusy()) {
        }

        leftFrontMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        rightFrontMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        leftRearMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        rightRearMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        stop()
    }

    fun stop() {
        leftFrontMotor.power = 0.0
        rightFrontMotor.power = 0.0
        leftRearMotor.power = 0.0
        rightRearMotor.power = 0.0
    }
}
