package org.firstinspires.ftc.teamcode;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.telemetry.TelemetryPacket;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

import java.util.List;

import static org.firstinspires.ftc.teamcode.roverruckus.LegacyConfiguration.USE_TELEMETRY_PACKET;

// 这个操作模式展示了如何识别在 2018~2019 赛季中的金矿石和银矿石。
@Disabled
@TeleOp(name = "概念: TensorFlow 物体识别", group = "概念")
public class TuXiangShiBie extends LinearOpMode {
    // 包含 TensorFlow 模型的资源的名字，模型驱动 TensorFlow 物体识别器识别矿石。
    private static final String TFOD_MODEL_ASSET = "RoverRuckus.tflite";
    // 上面的模型中金矿石使用的标签。
    private static final String LABEL_GOLD_MINERAL = "Gold Mineral";
    // 上面的模型中银矿石使用的标签。
    private static final String LABEL_SILVER_MINERAL = "Silver Mineral";

    // Vuforia 的授权密钥，它只有在有正确的密钥时才能启动。
    private static final String VUFORIA_KEY = "AR0/zTT/////AAAAmdIL8I+g30YLo3EYuz3yAVA07nKyaJEVgMhWll6Na0ZrAnVTbnCofSVje6SKFXqgLYoReTavM8NfIDaY9qhKBLrqLsgJRkQBEAhigOVa02Tm8L46Z1ARtAVwJCuMsT22966JlshpbxVXra2Vns+juHYRAEh7uI0jguxlmn1NbgEGw4HJJndCLntwnUMGHDuqS2r4PsBUBL+dVrhRfuOXfzG8A77/68LRSXiImK7UEAi2HR5FLTj/QyzPaV1bYbxIqLJcUEuqkZ92jFMGyL1HbxW2SaEjjZsoqd2hTmMum3oO5ZiLD9oX7T11aeCCzLBieV4eKFUnydvgnAIrTZYmLkDfZBHLa1Y1S7P2y8ZS1iCV";

    // 声明 Vuforia 定位器。在此，用来识别矿石的 TensorFlow 需要从 Vuforia 中获取摄像头拍摄的照片。
    private VuforiaLocalizer vuforia;

    // 声明 TensorFlow 物体识别器，它用来识别矿石。
    private TFObjectDetector tfod;

    @Override
    // 定义 runOpMode() 方法，当操控站上的 INIT 按钮被按下后，它会被调用。
    public void runOpMode() {
        FtcDashboard dashboard = FtcDashboard.getInstance();
        TelemetryPacket packet = new TelemetryPacket();

        // 用来识别矿石的 TensorFlow 需要从 Vuforia 中获得摄像头拍摄的照片，所以先让 Vuforia 准备好。
        initVuforia();

        // 检查现在使用的手机能不能使用 TensorFlow 来识别矿石。
        if (ClassFactory.getInstance().canCreateTFObjectDetector()) {
            // 手机能使用 TensorFlow，准备 TensorFlow 物体识别器。
            initTfod();
        } else {
            // 手机不能使用 TensorFlow，在遥测上提示。
            if (USE_TELEMETRY_PACKET) {
                packet.put("抱歉!", "此设备与 TFOD 不兼容");
            } else {
                telemetry.addData("抱歉!", "此设备与 TFOD 不兼容");
            }
        }

        // 在遥测上显示提示信息。
        if (USE_TELEMETRY_PACKET) {
            packet.put(">", "按“开始”以开始追踪");
        } else {
            telemetry.addData(">", "按“开始”以开始追踪");
        }
        // 更新遥测，让上面的信息显示。
        if (USE_TELEMETRY_PACKET) {
            dashboard.sendTelemetryPacket(packet);
        } else {
            telemetry.update();
        }
        // 等待操控站上的“开始”按钮被按下。当“开始”按钮按下后，waitForStart() 方法会立即返回。
        waitForStart();

        if (USE_TELEMETRY_PACKET) {
            packet = new TelemetryPacket();
        }

        // opModeIsActive() 方法检查这个 Op Mode 是否在运行。下面的部分只有在 Op Mode 正在运行时才会被执行。
        if (opModeIsActive()) {
            // 如果手机支持 TensorFlow 物体识别器，就激活它。
            if (tfod != null) {
                // 激活 TensorFlow 物体识别器。
                tfod.activate();
            }

            // 下面的部分只有在 Op Mode 正在运行的时候才会反复执行。
            while (opModeIsActive()) {
                // 如果手机支持 TensorFlow 物体识别器，就开始识别。
                if (tfod != null) {
                    // 如果在最后一次识别后到现在没有更改，下面的 getUpdatedRecognitions() 方法就不返回任何东西。
                    List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
                    // 如果识别到更改，就处理它们。
                    if (updatedRecognitions != null) {
                      // 在遥测上显示识别到的矿石的个数。
                      if (USE_TELEMETRY_PACKET) {
                        packet.put("被识别物体数量", updatedRecognitions.size());
                      } else {
                        telemetry.addData("被识别物体数量", updatedRecognitions.size());
                      }
                      // 只有在三个矿石都被识别到时才检查它们的位置。
                      if (updatedRecognitions.size() == 3) {
                        // 声明存放金矿石的位置的变量。
                        int goldMineralX = -1;
                        // 声明存放第一个银矿石的位置的变量。
                        int silverMineral1X = -1;
                        // 声明存放第二个银矿石的位置的变量。
                        int silverMineral2X = -1;
                        // 分别处理识别到的每一个矿石。
                        for (Recognition recognition : updatedRecognitions) {
                          // 注意：下面用来获得矿石位置的是 getRight() 方法，它返回识别到的物体的矩形边界的右座标。该系
                          // 列的其他方法包含 getLeft()、getTop() 与 getBottom()，它们分别返回边界的左座标、上座
                          // 标和下座标。

                          // 如果识别到的是金矿石，就：
                          if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                            // 记下金矿石的位置。
                            goldMineralX = (int) recognition.getRight();
                          }
                          // 否则，如果检测到的是银矿石且第一个银矿石还没被处理到，就：
                          else if (silverMineral1X == -1) {
                            // 记下第一个银矿石的位置。
                            silverMineral1X = (int) recognition.getRight();
                          }
                          // 金矿石和第一个银矿石的可能性都排除了，这次处理的是第二个银矿石：
                          else {
                            // 记下第二个银矿石的位置。
                            silverMineral2X = (int) recognition.getRight();
                          }
                        }
                        // 如果三个矿石都被识别到了，就：
                        if (goldMineralX != -1 && silverMineral1X != -1 && silverMineral2X != -1) {
                          // 如果金矿石在第一个和第二个银矿石前面，就可以确定金矿石在左边。
                          if (goldMineralX < silverMineral1X && goldMineralX < silverMineral2X) {
                            // 在遥测上显示金矿石在左边。
                            if (USE_TELEMETRY_PACKET) {
                              packet.put("金矿石位置", "左");
                            } else {
                              telemetry.addData("金矿石位置", "左");
                            }
                          }
                          // 否则，如果金矿石在第一个和第二个银矿石后面，就可以确定金矿石在右边。
                          else if (goldMineralX > silverMineral1X && goldMineralX > silverMineral2X) {
                            // 在遥测上显示金矿石在右边。
                            if (USE_TELEMETRY_PACKET) {
                              packet.put("金矿石位置", "右");
                            } else {
                              telemetry.addData("金矿石位置", "右");
                            }
                          }
                          // 金矿石在左边和右边的可能性都排除了，确定金矿石在中间：
                          else {
                            // 在遥测上显示金矿石在中间。
                            if (USE_TELEMETRY_PACKET) {
                              packet.put("金矿石位置", "中");
                            } else {
                              telemetry.addData("金矿石位置", "中");
                            }
                          }
                        }
                      }
                      // 更新遥测显示的数据。
                      if (USE_TELEMETRY_PACKET) {
                        dashboard.sendTelemetryPacket(packet);
                        packet = new TelemetryPacket();
                      } else {
                        telemetry.update();
                      }
                    }
                }
            }
        }

        // 如果手机支持 TensorFlow 物体识别器，就：
        if (tfod != null) {
            // 关闭 TensorFlow 物体识别器。
            tfod.shutdown();
        }
    }

    // 定义 initVuforia() 方法，它准备好 Vuforia 定位器。TensorFlow 物体识别器在识别矿石时需要从 Vuforia 中获取摄像
    // 头拍摄的照片。
    private void initVuforia() {
        // 新建一个 Parameters 变量，Vuforia 要求通过它来保存设置。在新建 Vuforia 定位器时，会将这个 Parameters
        // 变量传进去。
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();

        // 设定 Vuforia 的授权密钥，Vuforia 只有在使用正确的密钥时才能启动。
        parameters.vuforiaLicenseKey = VUFORIA_KEY;
        // 设定 Vuforia 要使用的摄像头，这里决定让它用后置摄像头。
        parameters.cameraDirection = CameraDirection.FRONT;

        // 新建 Vuforia 定位器并将上面设定的 Parameters 变量传进去。
        vuforia = ClassFactory.getInstance().createVuforia(parameters);
    }

    // 定义 initTfod() 方法，它准备好 TensorFlow 物体识别器，用来识别矿石。
    private void initTfod() {
        // 获得用来显示 TensorFlow 的界面的标识符。
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
            "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        // 新建一个 Parameters 变量。此处的 Parameters 不同于 initVuforia() 方法中的 Parameters，它是为保存
        // TensorFlow 设置而不是为 Vuforia 保存设置而准备的。tfodMonitorViewId 指定了 Parameters 中的显示界面标
        // 识符。
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        // 新建一个 TensorFlow 物体识别器并将上面设定的 Parameters 变量传进去。
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        // 向 TensorFlow 物体识别器加载模型来识别矿石。在这里，loadModelFromAsset() 从资源中加载模型。
        // TFOD_MODEL_ASSET 指出了模型所在的资源的名字，LABEL_GOLD_MINERAL 指出了模型中金矿石所使用的标签，
        // LABEL_SILVER_MINERAL 指出了模型中银矿石所使用的标签。
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_GOLD_MINERAL, LABEL_SILVER_MINERAL);
    }
}
