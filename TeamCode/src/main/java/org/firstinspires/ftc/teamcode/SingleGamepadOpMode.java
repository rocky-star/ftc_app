package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.roverruckus.RoverRuckusRobot;

@Disabled
@TeleOp(name = "Single Gamepad", group = "Testing")
public class SingleGamepadOpMode extends OpMode {
    private RoverRuckusRobot robot;

    @Override
    public void init() {
        robot = new RoverRuckusRobot(hardwareMap);
        robot.reset();
    }

    @Override
    public void loop() {
        robot.driveWithMecanumWheels(
                (double) Math.hypot(gamepad1.left_stick_x, gamepad1.left_stick_y),
                Math.atan2(gamepad1.left_stick_y, gamepad1.left_stick_x)  - Math.PI / 4,
                (double) gamepad1.right_stick_x
        );
    }
}
