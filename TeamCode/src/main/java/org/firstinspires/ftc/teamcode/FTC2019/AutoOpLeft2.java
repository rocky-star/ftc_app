package org.firstinspires.ftc.teamcode.FTC2019;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

/**
 * Created by my on 2018/2/22.
 */
@TeleOp(name = "AutoOpLeft2")
public class AutoOpLeft2 extends AutoOpBase {
    @Override
    public void runOpMode() throws InterruptedException {
        initBot();
        waitForStart();

        driveLeftDistance(5, 0.2, 0.2, 0.8, 0.8);
        while (opModeIsActive()) {
            idle();
        }
    }
}
