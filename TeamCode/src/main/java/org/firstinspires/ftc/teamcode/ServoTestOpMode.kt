package org.firstinspires.ftc.teamcode

import com.qualcomm.robotcore.eventloop.opmode.Disabled
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode
import com.qualcomm.robotcore.eventloop.opmode.TeleOp

@Disabled
@TeleOp(name = "Servo", group = "Testing")
class ServoTestOpMode : LinearOpMode() {
    override fun runOpMode() {
        val servo = hardwareMap.servo.get("s")
        waitForStart()
        while (opModeIsActive()) {
            if (gamepad1.a) {
                sleep(1000)
                servo.position += 0.1
            } else if (gamepad1.b) {
                sleep(1000)
                servo.position -= 0.1
            }
            telemetry.addData("Position", servo.position)
            telemetry.update()
        }
    }
}