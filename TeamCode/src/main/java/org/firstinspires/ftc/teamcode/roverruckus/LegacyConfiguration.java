package org.firstinspires.ftc.teamcode.roverruckus;

import com.acmerobotics.dashboard.config.Config;

@Config
public class LegacyConfiguration {
    public static double JOYSTICK_THRESHOLD = 0.2;
    public static double DRAWING_FACTOR = 0.1;
    public static boolean USE_TELEMETRY_PACKET = true;
    public static double LEFT_FRONT_FACTOR = 1.0;
    public static double RIGHT_FRONT_FACTOR = 1.0;
    public static double LEFT_REAR_FACTOR = 1.0;
    public static double RIGHT_REAR_FACTOR = 1.0;
    public static int COUNTS_FACTOR = 2;
}
