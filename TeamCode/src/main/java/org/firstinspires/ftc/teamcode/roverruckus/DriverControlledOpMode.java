package org.firstinspires.ftc.teamcode.roverruckus;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.telemetry.TelemetryPacket;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

import static org.firstinspires.ftc.teamcode.roverruckus.AutonomousConfiguration.AUTONOMOUS_CHASSIS_POWER;
import static org.firstinspires.ftc.teamcode.roverruckus.AutonomousConfiguration.COUNTS_PER_ROUNDS;
import static org.firstinspires.ftc.teamcode.roverruckus.InputConfiguration.JOYSTICK_THRESHOLD;
import static org.firstinspires.ftc.teamcode.roverruckus.InputConfiguration.KEY_PRESSING_DELAY;

@Disabled
@TeleOp(name = "Driver Controlled", group = "Competition")
public class DriverControlledOpMode extends OpMode {
    private enum ExecutorState {
        RAISING, LOWERING;

        public final ExecutorState inverted() {
            return this == RAISING ? LOWERING : RAISING;
        }
    }

    private static FtcDashboard dashboard = FtcDashboard.getInstance();
    private RoverRuckusRobot robot;
    private ExecutorState dropperState = ExecutorState.LOWERING;
    private ExecutorState collectorState = ExecutorState.LOWERING;
    private TelemetryPacket packet = new TelemetryPacket();

    private void waitForKeyReleased() {
        try {
            Thread.sleep(KEY_PRESSING_DELAY);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public final void init() {
        robot = new RoverRuckusRobot(hardwareMap);
    }

    @Override
    public final void start() {
        robot.reset();
    }

    @Override
    public final void loop() {
        double power = Math.hypot(gamepad1.left_stick_x, gamepad1.left_stick_y);
        packet.put("Power", power);

        if (gamepad1.left_stick_x > JOYSTICK_THRESHOLD
                && gamepad1.left_stick_y < -JOYSTICK_THRESHOLD) {
            robot.driveNE(power);
            packet.put("Direction", "Northeast");
        } else if (gamepad1.left_stick_x > JOYSTICK_THRESHOLD
                   && gamepad1.left_stick_y > JOYSTICK_THRESHOLD) {
            robot.driveSE(power);
            packet.put("Direction", "Southeast");
        } else if (gamepad1.left_stick_x < -JOYSTICK_THRESHOLD
                   && gamepad1.left_stick_y > JOYSTICK_THRESHOLD) {
            robot.driveSW(power);
            packet.put("Direction", "Southwest");
        } else if (gamepad1.left_stick_x < -JOYSTICK_THRESHOLD
                   && gamepad1.left_stick_y < -JOYSTICK_THRESHOLD) {
            robot.driveNW(power);
            packet.put("Direction", "Northwest");
        } else if (gamepad1.left_stick_x > JOYSTICK_THRESHOLD) {
            robot.driveE(power);
            packet.put("Direction", "East");
        } else if (gamepad1.left_stick_x < -JOYSTICK_THRESHOLD) {
            robot.driveW(power);
            packet.put("Direction", "West");
        } else if (gamepad1.left_stick_y > JOYSTICK_THRESHOLD) {
            robot.driveS(power);
            packet.put("Direction", "South");
        } else if (gamepad1.left_stick_y < -JOYSTICK_THRESHOLD) {
            robot.driveN(power);
            packet.put("Direction", "North");
        } else {
            robot.stopChassis();
            packet.put("Direction", "Stop");
        }

        double rotationAngle = gamepad1.right_stick_x;
        packet.put("Rotation Angle", rotationAngle);
        if (Math.abs(rotationAngle) > JOYSTICK_THRESHOLD) {
            robot.driveWithMecanumWheels(Math.abs(rotationAngle), 0.0, -rotationAngle);
        }

        if (gamepad2.left_bumper) {
            waitForKeyReleased();
            if (dropperState == ExecutorState.RAISING) {
                robot.lowerDropper();
                packet.put("Dropper", "Lowered");
            } else {
                robot.raiseDropper();
                packet.put("Dropper", "Raised");
            }
            dropperState = dropperState.inverted();
        }
        double dropperPower = -gamepad2.left_stick_y;
        robot.setDropperPower(dropperPower);
        packet.put("Dropper Power", dropperPower);

        if (gamepad2.right_bumper) {
            waitForKeyReleased();
            if (collectorState == ExecutorState.RAISING) {
                robot.lowerCollector();
                packet.put("Collector", "Lowered");
            } else {
                robot.raiseCollector();
                packet.put("Collector", "Raised");
            }
            collectorState = collectorState.inverted();
        }
        //double collectorPower = -gamepad2.right_stick_y;
        //robot.setCollectorPower(collectorPower);
        //packet.put("Collector Power", collectorPower);

        if (gamepad2.a) {
            waitForKeyReleased();
            robot.startMineralCollection();
            packet.put("Mineral Collection", "Started");
        } else if (gamepad2.b) {
            waitForKeyReleased();
            robot.stopMineralCollection();
            packet.put("Mineral Collection", "Stopped");
        }

        double robotConnectionPower = -gamepad2.right_stick_y;
        robot.setRobotConnectionDirection(DcMotorSimple.Direction.FORWARD);
        robot.setRobotConnectionPower(robotConnectionPower);
        packet.put("Robot Connection Power", robotConnectionPower);

        if (gamepad2.x) {
            robot.startRobotConnectionWithDuration();
            packet.put("Robot Connection Duration", "Started");
        }

        dashboard.sendTelemetryPacket(packet);
    }

    @Override
    public final void stop() {
        robot.stopChassis();
    }
}
