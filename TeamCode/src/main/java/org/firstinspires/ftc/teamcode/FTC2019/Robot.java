package org.firstinspires.ftc.teamcode.FTC2019;

import android.graphics.Color;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;

import static java.lang.Thread.sleep;

/**
 * Created by my on 2018/2/22.
 */

public class Robot {


    static final double COUNTS_PER_MOTOR_REV = 1680;
    static final double DRIVE_GEAR_REDUCTION = 1.0;
    static final double WHEEL_DIAMETER_INCHES = 1.0;
    static final double COUNTS_PER_INCH = (COUNTS_PER_MOTOR_REV * DRIVE_GEAR_REDUCTION) / (WHEEL_DIAMETER_INCHES * Math.PI);


    //  Vurforia 需要相关de类
    VuforiaLocalizer vuforia;
    VuforiaTrackables relicTrackables;
    VuforiaTrackable relicTemplate;


    //  底盘
    DcMotor leftFront;
    DcMotor rightFront;
    DcMotor rightBack;
    DcMotor leftBack;
    //    横向
    public DcMotor x;
    //    纵向
    public DcMotor y;
    //    挂车
    public DcMotor g;

    Servo ff;
    Servo bf;
    CRServo vex;

    Telemetry telemetry;

    HardwareMap hardwareMap = null;

    //    构造函数
    public Robot() {

    }

    //    初始化硬件
    public void init(HardwareMap hardwareMap, Telemetry telemetry) throws InterruptedException {
//       保存硬件映射
        this.hardwareMap = hardwareMap;
        this.telemetry = telemetry;

//底盘四个电机
        rightFront = hardwareMap.dcMotor.get("rf");
        leftFront = hardwareMap.dcMotor.get("lf");
        rightBack = hardwareMap.dcMotor.get("rb");
        leftBack = hardwareMap.dcMotor.get("lb");

        vex = hardwareMap.crservo.get("v");
        ff = hardwareMap.servo.get("ff");
        bf = hardwareMap.servo.get("bf");

        x = hardwareMap.dcMotor.get("x");
        y = hardwareMap.dcMotor.get("y");
        g = hardwareMap.dcMotor.get("g");

//      设置初识功率为0
        stopDriving();
    }

    //    重置编码器
    void resetEncoders() {
        leftFront.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightFront.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightBack.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftBack.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
    }

    //     设置通过编码器让机器人行驶
    void setUseEncoderMode() {
        leftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

    void setMotorStopMode() {
        leftFront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightFront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightBack.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftBack.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    }

    //      设置编码器让他到指定位置
    void setRunToPositionMode() {
        leftFront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rightFront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rightBack.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        leftBack.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }

    /**
     * 直行相关方法
     */

    //    设置行走功率
    void driveForward(double power) {
        driveForward(power, power, power, power);
    }

    //    设置行走方向
    void driveForward(double leftFrontPower, double rightFrontPower, double rightBackPower, double leftBackPower) {

        leftFront.setPower(leftFrontPower);
        leftBack.setPower(leftBackPower);
        rightFront.setPower(rightFrontPower);
        rightBack.setPower(rightBackPower);
    }

    /**
     * 后退相关方法
     */


    //    设置方向行走
    void driveBackward(double power) {
        driveForward(-power);
    }

    void driveBackward(double leftFrontPower, double rightFrontPower, double rightBackPower, double leftBackPower) {

        leftFront.setPower(leftFrontPower);
        leftBack.setPower(leftBackPower);
        rightFront.setPower(rightFrontPower);
        rightBack.setPower(rightBackPower);
    }

    /**
     * 左平移
     */

    //    设置左平移行走功率
    void driveLeft(double power) {
        driveLeft(power, -power, power, -power);
    }

    //    设置左平移方向
    void driveLeft(double leftFrontPower, double rightFrontPower, double rightBackPower, double leftBackPower) {

        leftFront.setPower(leftFrontPower);
        leftBack.setPower(leftBackPower);
        rightFront.setPower(rightFrontPower);
        rightBack.setPower(rightBackPower);
    }

    /**
     * 右平移
     */

    //    设置右平移行走功率
    void driveRight(double power) {
        driveRight(power, power, power, power);
    }

    //    设置右平移方向
    void driveRight(double leftFrontPower, double rightFrontPower, double rightBackPower, double leftBackPower) {

        leftFront.setPower(leftFrontPower);
        leftBack.setPower(leftBackPower);
        rightFront.setPower(rightFrontPower);
        rightBack.setPower(rightBackPower);
    }


    /**
     * 左转相关方法
     */


    void autoOpTurnLeft(double power) {
        leftFront.setPower(-power);
        rightFront.setPower(-power);
        rightBack.setPower(-power);
        leftBack.setPower(-power);
    }

    /**
     * 右转相关方法
     */

    //    原地右转
    void turnRight(double power) {

        leftFront.setPower(power);
        rightFront.setPower(power);
        rightBack.setPower(power);
        leftBack.setPower(power);
    }

    //    停止行驶
    void stopDriving() {
        driveForward(0);
    }


    //    是否是运动状态
    boolean isBusy() {
        return leftFront.isBusy()
                && rightFront.isBusy()
                && rightBack.isBusy()
                && leftBack.isBusy();
    }
}
