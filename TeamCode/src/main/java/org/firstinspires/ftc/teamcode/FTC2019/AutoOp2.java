package org.firstinspires.ftc.teamcode.FTC2019;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

/**
 * Created by my on 2018/2/22.
 */
@TeleOp(name = "AutoOp2")
public class AutoOp2 extends AutoOpBase {

    static final double FF_UP = 1;
    static final double FF_DOWN = 0.24;
    static final double BF_UP = 0.5;
    static final double BF_DOWN = 0.89;


    @Override
    public void runOpMode() throws InterruptedException {
        initBot();
        waitForStart();
//
        driveForwardDistance(2, 0.3, 0.3, 0.5, 0.5);
        while (opModeIsActive()) {
            idle();
        }

        //FrontUP();

    }

    private void FrontUP() {
        bot.ff.setPosition(FF_UP);

    }

    private void FrontDOWN() {
        bot.ff.setPosition(FF_DOWN);

    }

    private void BackUP() {
        bot.bf.setPosition(BF_UP);

    }

    private void BackDOWN() {
        bot.bf.setPosition(BF_DOWN);

    }

}
