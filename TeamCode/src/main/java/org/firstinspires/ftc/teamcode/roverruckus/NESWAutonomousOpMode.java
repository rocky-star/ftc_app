package org.firstinspires.ftc.teamcode.roverruckus;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.BaseRobot;

import static org.firstinspires.ftc.teamcode.roverruckus.AutonomousConfiguration.AS1_FORWARD_ROUNDS;
import static org.firstinspires.ftc.teamcode.roverruckus.AutonomousConfiguration.AS2_SIDE_WAY_ROUNDS;
import static org.firstinspires.ftc.teamcode.roverruckus.AutonomousConfiguration.AS3_FORWARD_ROUNDS;
import static org.firstinspires.ftc.teamcode.roverruckus.AutonomousConfiguration.AS4_TURN_ROUND_ROUNDS;
import static org.firstinspires.ftc.teamcode.roverruckus.AutonomousConfiguration.AS5_BACKWARD_ROUNDS;
import static org.firstinspires.ftc.teamcode.roverruckus.AutonomousConfiguration.AUTONOMOUS_CHASSIS_POWER;
import static org.firstinspires.ftc.teamcode.roverruckus.AutonomousConfiguration.COUNTS_PER_ROUNDS;

public class NESWAutonomousOpMode extends LinearOpMode {
    private int getCounts(double rounds) {
        return (int) (rounds * COUNTS_PER_ROUNDS);
    }

    @Override
    public void runOpMode() throws InterruptedException {
        RoverRuckusRobot robot = new RoverRuckusRobot(hardwareMap);

        //robot.initializeVuforiaLocalizer();
        //robot.initializeTFObjectDetector();
        waitForStart();
        robot.reset();

        if (opModeIsActive()) {
            //BaseRobot.MineralLocation mineralLocation = robot.detectMineralLocation();
            robot.driveForward(AUTONOMOUS_CHASSIS_POWER, getCounts(AS1_FORWARD_ROUNDS));

            //if (mineralLocation == BaseRobot.MineralLocation.LEFT) {
                //robot.driveLeftSideWay(AUTONOMOUS_CHASSIS_POWER, getCounts(AS2_SIDE_WAY_ROUNDS));
                //robot.driveForward(AUTONOMOUS_CHASSIS_POWER, getCounts(AS3_FORWARD_ROUNDS));
                //robot.turnRoundClockwise(AUTONOMOUS_CHASSIS_POWER, getCounts(AS4_TURN_ROUND_ROUNDS));
            //} else if (mineralLocation == BaseRobot.MineralLocation.RIGHT) {
                //robot.driveRightSideWay(AUTONOMOUS_CHASSIS_POWER, getCounts(AS2_SIDE_WAY_ROUNDS));
                //robot.driveForward(AUTONOMOUS_CHASSIS_POWER, getCounts(AS3_FORWARD_ROUNDS));
                //robot.turnRoundAnticlockwise(AUTONOMOUS_CHASSIS_POWER, getCounts(AS4_TURN_ROUND_ROUNDS));
            //}

            robot.driveBackward(AUTONOMOUS_CHASSIS_POWER, getCounts(AS5_BACKWARD_ROUNDS));
            //robot.shutdownTFObjectDetector();
        }
    }
}
