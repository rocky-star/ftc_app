package org.firstinspires.ftc.teamcode.FTC2019;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

@TeleOp(name = "手动模式")
public class ManualOpMode extends LinearOpMode {
    static final double COUNTS_PER_MOTOR_REV = 1120;
    static final double DRIVE_GEAR_REDUCTION = 1.0;
    static final double WHEEL_DIAMETER_INCHES = 4.0;
    static final double COUNTS_PER_INCH = (COUNTS_PER_MOTOR_REV * DRIVE_GEAR_REDUCTION) / (WHEEL_DIAMETER_INCHES * Math.PI);

    DcMotor rightFront;
    DcMotor leftFront;
    DcMotor rightBack;
    DcMotor leftBack;

    CRServo vex;
    Servo ff;
    Servo bf;

    DcMotor x;
    DcMotor y;
    DcMotor g;

    boolean tlOdd = true;

    private void waitForKey() {
        sleep(250);
    }

    void setRunToPositionMode() {
        rightFront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        leftFront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rightBack.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        leftBack.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }

    boolean isBusy() {
        return leftFront.isBusy()
                && rightFront.isBusy()
                && rightBack.isBusy()
                && leftBack.isBusy();
    }

    void stopDriving() {
        driveForward(0, 0, 0, 0);
    }


    void driveLeft(double leftFrontPower, double rightFrontPower, double rightBackPower, double leftBackPower) {

        leftFront.setPower(leftFrontPower);
        leftBack.setPower(leftBackPower);
        rightFront.setPower(rightFrontPower);
        rightBack.setPower(rightBackPower);
    }

    void driveRight(double leftFrontPower, double rightFrontPower, double rightBackPower, double leftBackPower) {

        leftFront.setPower(leftFrontPower);
        leftBack.setPower(leftBackPower);
        rightFront.setPower(rightFrontPower);
        rightBack.setPower(rightBackPower);
    }

    void driveForward(double leftFrontPower, double rightFrontPower, double rightBackPower, double leftBackPower) {

        leftFront.setPower(leftFrontPower);
        leftBack.setPower(leftBackPower);
        rightFront.setPower(rightFrontPower);
        rightBack.setPower(rightBackPower);
    }

    //     设置通过编码器让机器人行驶
    void setUseEncoderMode() {
        leftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

    void setMotorStopMode() {
        leftFront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightFront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightBack.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftBack.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    }

    void driveLeftDistance(int leftInches, double leftFrontpower, double rightFrontPower, double rightBackPower, double leftBackPower) {

//         设置4个电机转动的目标
        int newLeftFrontTarget;
        int newRightFrontTarget;
        int newRightBackTarget;
        int newLeftBackTarget;


        if (opModeIsActive()) {

            newLeftFrontTarget = leftFront.getCurrentPosition() - (int) (leftInches * COUNTS_PER_INCH);
            newRightFrontTarget = rightFront.getCurrentPosition() - (int) (leftInches * COUNTS_PER_INCH);
            newRightBackTarget = rightBack.getCurrentPosition() + (int) (leftInches * COUNTS_PER_INCH);
            newLeftBackTarget = leftBack.getCurrentPosition() + (int) (leftInches * COUNTS_PER_INCH);

//            让车行驶至目标
            leftFront.setTargetPosition(newLeftFrontTarget);
            rightFront.setTargetPosition(newRightFrontTarget);
            rightBack.setTargetPosition(newRightBackTarget);
            leftBack.setTargetPosition(newLeftBackTarget);

//             设置行驶模式
            setRunToPositionMode();
//             设置行驶速度
            driveLeft(leftFrontpower, rightFrontPower, rightBackPower, leftBackPower);

            while (opModeIsActive() && isBusy()) {

            }

//        设置电机停止模式
//            setMotorStopMode();
//          停止移动
            stopDriving();
//          设置编码器
            setUseEncoderMode();
        }
    }


//    /**
//     * 右平移至目标
//     *
//     * @param forwardInches 目标距离
//     * @param driveSpeed    行驶速度
//     */

    void driveRightDistance(double forwardInches, double leftFrontPower, double rightFrontPower, double rightBackPower, double leftBackPower) {

//         设置4个电机转动的目标
        int newLeftFrontTarget;
        int newRightFrontTarget;
        int newRightBackTarget;
        int newLeftBackTarget;


        if (opModeIsActive()) {

            newLeftFrontTarget = leftFront.getCurrentPosition() + (int) (forwardInches * COUNTS_PER_INCH);
            newRightFrontTarget = rightFront.getCurrentPosition() + (int) (forwardInches * COUNTS_PER_INCH);
            newRightBackTarget = rightBack.getCurrentPosition() - (int) (forwardInches * COUNTS_PER_INCH);
            newLeftBackTarget = leftBack.getCurrentPosition() - (int) (forwardInches * COUNTS_PER_INCH);

//            让车行驶至目标
            leftFront.setTargetPosition(newLeftFrontTarget);
            rightFront.setTargetPosition(newRightFrontTarget);
            rightBack.setTargetPosition(newRightBackTarget);
            leftBack.setTargetPosition(newLeftBackTarget);

//             设置行驶模式
            setRunToPositionMode();
//             设置行驶速度
            driveRight(leftFrontPower, rightFrontPower, rightBackPower, leftBackPower);

            while (opModeIsActive() && isBusy()) {

            }

//        设置电机停止模式
//            setMotorStopMode();
//          停止移动
            stopDriving();
//          设置编码器
            setUseEncoderMode();
        }
    }

    void turnLeftDistance(double forwardInches, double leftFrontPower, double rightFrontPower, double rightBackPower, double leftBackPower) {
//         设置4个电机转动的目标
        int newLeftFrontTarget;
        int newRightFrontTarget;
        int newRightBackTarget;
        int newLeftBackTarget;


        if (opModeIsActive()) {

            newLeftFrontTarget = leftFront.getCurrentPosition() - (int) (forwardInches * COUNTS_PER_INCH);
            newRightFrontTarget = rightFront.getCurrentPosition() - (int) (forwardInches * COUNTS_PER_INCH);
            newRightBackTarget = rightBack.getCurrentPosition() - (int) (forwardInches * COUNTS_PER_INCH);
            newLeftBackTarget = leftBack.getCurrentPosition() - (int) (forwardInches * COUNTS_PER_INCH);

//            让车行驶至目标
            leftFront.setTargetPosition(newLeftFrontTarget);
            rightFront.setTargetPosition(newRightFrontTarget);
            rightBack.setTargetPosition(newRightBackTarget);
            leftBack.setTargetPosition(newLeftBackTarget);

//             设置行驶模式
            setRunToPositionMode();
//             设置行驶速度
            driveForward(leftFrontPower, rightFrontPower, rightBackPower, leftBackPower);

            while (opModeIsActive() && isBusy()) {

            }

//        设置电机停止模式
            setMotorStopMode();
//          停止移动
            stopDriving();
//          设置编码器
            setUseEncoderMode();
        }
    }

    void turnRightDistance(double forwardInches, double leftFrontPower, double rightFrontPower, double rightBackPower, double leftBackPower) {
//         设置4个电机转动的目标
        int newLeftFrontTarget;
        int newRightFrontTarget;
        int newRightBackTarget;
        int newLeftBackTarget;


        if (opModeIsActive()) {

            newLeftFrontTarget = leftFront.getCurrentPosition() + (int) (forwardInches * COUNTS_PER_INCH);
            newRightFrontTarget = rightFront.getCurrentPosition() + (int) (forwardInches * COUNTS_PER_INCH);
            newRightBackTarget = rightBack.getCurrentPosition() + (int) (forwardInches * COUNTS_PER_INCH);
            newLeftBackTarget = leftBack.getCurrentPosition() + (int) (forwardInches * COUNTS_PER_INCH);

//            让车行驶至目标
            leftFront.setTargetPosition(newLeftFrontTarget);
            rightFront.setTargetPosition(newRightFrontTarget);
            rightBack.setTargetPosition(newRightBackTarget);
            leftBack.setTargetPosition(newLeftBackTarget);

//             设置行驶模式
            setRunToPositionMode();
//             设置行驶速度
            driveForward(leftFrontPower, rightFrontPower, rightBackPower, leftBackPower);

            while (opModeIsActive() && isBusy()) {

            }

//        设置电机停止模式
            setMotorStopMode();
//          停止移动
            stopDriving();
//          设置编码器
            setUseEncoderMode();
        }
    }

    @Override
    public void runOpMode() throws InterruptedException {
        rightFront = hardwareMap.dcMotor.get("rf");
        leftFront = hardwareMap.dcMotor.get("lf");
        rightBack = hardwareMap.dcMotor.get("rb");
        leftBack = hardwareMap.dcMotor.get("lb");

        vex = hardwareMap.crservo.get("v");
        ff = hardwareMap.servo.get("ff");
        bf = hardwareMap.servo.get("bf");

        x = hardwareMap.dcMotor.get("x");
        y = hardwareMap.dcMotor.get("y");
        g = hardwareMap.dcMotor.get("g");

        rightFront.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftFront.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightBack.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftBack.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        rightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        waitForStart();
        stopDriving();
        bf.setPosition(AutoOpTest.BF_DOWN);
        ff.setPosition(AutoOpTest.FF_UP);
        rightFront.setPower(0.0);
        leftFront.setPower(0.0);
        rightBack.setPower(0.0);
        leftBack.setPower(0.0);
        while (opModeIsActive()) {
            double r = Math.hypot(gamepad1.left_stick_x, gamepad1.left_stick_y);
            double robotAngle = Math.atan2(-gamepad1.left_stick_x, gamepad1.left_stick_y) - Math.PI / 4;
            double rightX = gamepad1.right_stick_x;
            final double v1 = -r * Math.cos(robotAngle) + rightX;
            final double v2 = -r * Math.sin(robotAngle) + rightX;
            final double v3 = r * Math.sin(robotAngle) + rightX;
            final double v4 = r * Math.cos(robotAngle) + rightX;
            leftFront.setPower(v1);
            rightFront.setPower(v2);
            leftBack.setPower(v3);
            rightBack.setPower(v4);

            if (gamepad1.x) {
                driveLeftDistance(2, 0.3, 0.3, 0.5, 0.5);
            } else if (gamepad1.b)  {
                driveRightDistance(2, 0.3, 0.3, 0.5, 0.5);
            }
            if (gamepad1.y) {
                turnLeftDistance(2, 0.3, 0.3, 0.5, 0.5);
            } else if (gamepad1.a) {
                turnRightDistance(2, 0.3, 0.3, 0.5, 0.5);
            }

            if (gamepad1.dpad_up) {
                bf.setPosition(bf.getPosition() + 0.1);
            } else if (gamepad1.dpad_down) {
                bf.setPosition(bf.getPosition() - 0.1);
            }

            if (gamepad1.left_bumper) {
                ff.setPosition(AutoOpTest.FF_DOWN);
            } else if (gamepad1.right_bumper) {
                ff.setPosition(AutoOpTest.FF_UP);
            }

            if (gamepad1.dpad_left) {
                ff.setPosition(AutoOpTest.FF_MIDDLE);
            }

            x.setPower(gamepad2.left_stick_y);
            y.setPower(gamepad2.right_stick_y);

            if (gamepad2.left_bumper) {
                bf.setPosition(AutoOpTest.BF_UP);
            } else if (gamepad2.right_bumper) {
                bf.setPosition(AutoOpTest.BF_DOWN);
            }

            while (gamepad2.x) {
                vex.setPower(0.8);
            }
            while (gamepad2.b) {
                vex.setPower(-0.8);
            }
            vex.setPower(0.0);

            if (gamepad2.dpad_left) {
                turnLeftDistance(8, 0.3, 0.3, 0.5, 0.5);
            }

            g.setPower(-gamepad2.left_trigger);
            if (gamepad2.right_trigger != 0.0F) {
                g.setPower(gamepad2.right_trigger);
            }
            if (gamepad2.a) {
                g.setPower(1.0);
            }
        }
        stopDriving();
        rightFront.setPower(0.0);
        leftFront.setPower(0.0);
        rightBack.setPower(0.0);
        leftBack.setPower(0.0);
        g.setPower(0.0);
    }
}
