package org.firstinspires.ftc.teamcode.FTC2019;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import static org.firstinspires.ftc.teamcode.FTC2019.AutoOpTest.BF_DOWN;
import static org.firstinspires.ftc.teamcode.FTC2019.AutoOpTest.BF_UPUP;

@Autonomous(name = "主位红")
public class ZhuweiHongOpMode extends AutoOpBase {
    private static final double LF_POWER = 0.4;
    private static final double RF_POWER = 0.6;
    private static final double LB_POWER = 0.6;
    private static final double RB_POWER = 0.6;

    @Override
    public void runOpMode() throws InterruptedException {
        initBot();
        bot.g.setPower(1.0);
        waitForStart();
        if (opModeIsActive()) {
            bot.g.setPower(-1.0);
            sleep(1000);
            driveRightDistance(0.5, LF_POWER, RF_POWER, LB_POWER, RB_POWER);
            bot.g.setPower(0.0);

            bot.bf.setPosition(AutoOpTest.BF_DOWN);
            bot.ff.setPosition(AutoOpTest.FF_UP);
            sleep(500);

            //bot.g.setPower(0.5);
            //sleep(3000);
            //driveRightDistance(10, LF_POWER, RF_POWER, RB_POWER, LB_POWER);
            //bot.g.setPower(-0.5);
            //sleep(3000);
            //bot.g.setPower(0.0);

            telemetry.addLine("1. Forward");
            telemetry.update();
//            driveForwardDistance(5, LF_POWER, RF_POWER, RB_POWER, LB_POWER);
            driveForwardDistance(3.75, LF_POWER, RF_POWER, RB_POWER, LB_POWER);
            sleep(2000);

            bot.g.setPower(1.0);
            sleep(2000);
            bot.g.setPower(0.0);

            telemetry.addLine("2. Backward");
            telemetry.update();
//            driveBackwardDistance(2, LF_POWER, RF_POWER, RB_POWER, LB_POWER);
            driveBackwardDistance(2, LF_POWER, RF_POWER, RB_POWER, LB_POWER);
            telemetry.addLine("3. Left Side Way");
            telemetry.update();
            bot.x.setPower(-1.0);
            sleep(600);
            bot.x.setPower(0.0);
//            driveLeftDistance(9, 0.4, 0.4, 0.6, 0.6);
            driveLeftDistance(5.75, 0.4, 0.4, 0.6, 0.6);
//            driveBackwardDistance(1, 0.6, 0.6, 0.8, 0.8);
            //bot.x.setPower(1.0);
            //sleep(750);
            //bot.x.setPower(0.0);
            //bot.ff.setPosition(AutoOpTest.FF_DOWN);
            bot.y.setPower(-1.0);
            sleep(2000);
            bot.bf.setPosition(BF_UPUP);
            sleep(1000);
            bot.bf.setPosition(BF_DOWN);
            sleep(500);
            bot.y.setPower(1.0);
            sleep(5000);
            bot.y.setPower(0.0);
            bot.x.setPower(1.0);
            sleep(1000);
            bot.x.setPower(0.0);
            bot.resetEncoders();
            bot.setUseEncoderMode();
//            driveForwardDistance(4, LF_POWER, RF_POWER, RB_POWER, LB_POWER);
            driveLeftDistance(5, 0.8, 0.8, 0.5, 0.5);
            driveForwardDistance(1.5, LF_POWER, RF_POWER, RB_POWER, LB_POWER);
        }
        while (opModeIsActive()) {
            idle();
        }
    }
}
