package org.firstinspires.ftc.teamcode.roverruckus;

import com.acmerobotics.dashboard.config.Config;

@Config
public class RobotConnectionConfiguration {
    public static double ROBOT_CONNECTION_DURATION_POWER = 0.2;
    public static int ROBOT_CONNECTION_DURATION_TIME = 10000;
}
