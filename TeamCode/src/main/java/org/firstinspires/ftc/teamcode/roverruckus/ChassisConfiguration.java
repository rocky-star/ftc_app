package org.firstinspires.ftc.teamcode.roverruckus;

import com.acmerobotics.dashboard.config.Config;

@Config
public class ChassisConfiguration {
    public static double LEFT_FRONT_POWER_FACTOR = 1.0;
    public static double RIGHT_FRONT_POWER_FACTOR = 1.0;
    public static double LEFT_REAR_POWER_FACTOR = 1.0;
    public static double RIGHT_REAR_POWER_FACTOR = 1.0;
}
