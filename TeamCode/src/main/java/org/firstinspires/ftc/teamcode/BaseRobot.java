package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

import java.util.List;

import static org.firstinspires.ftc.teamcode.roverruckus.ChassisConfiguration.LEFT_FRONT_POWER_FACTOR;
import static org.firstinspires.ftc.teamcode.roverruckus.ChassisConfiguration.LEFT_REAR_POWER_FACTOR;
import static org.firstinspires.ftc.teamcode.roverruckus.ChassisConfiguration.RIGHT_FRONT_POWER_FACTOR;
import static org.firstinspires.ftc.teamcode.roverruckus.ChassisConfiguration.RIGHT_REAR_POWER_FACTOR;

public class BaseRobot {
    public enum MineralLocation { LEFT, CENTER, RIGHT };

    private static final String VUFORIA_LICENSE_KEY = (
        "AR0/zTT/////AAAAmdIL8I+g30YLo3EYuz3yAVA07nKyaJEVgMhWll6Na0ZrAnVTbnCofSVje6"
        + "SKFXqgLYoReTavM8NfIDaY9qhKBLrqLsgJRkQBEAhigOVa02Tm8L46Z1ARtAVwJCuMsT2296"
        + "6JlshpbxVXra2Vns+juHYRAEh7uI0jguxlmn1NbgEGw4HJJndCLntwnUMGHDuqS2r4PsBUBL"
        + "+dVrhRfuOXfzG8A77/68LRSXiImK7UEAi2HR5FLTj/QyzPaV1bYbxIqLJcUEuqkZ92jFMGyL"
        + "1HbxW2SaEjjZsoqd2hTmMum3oO5ZiLD9oX7T11aeCCzLBieV4eKFUnydvgnAIrTZYmLkDfZB"
        + "HLa1Y1S7P2y8ZS1iCV");

    protected HardwareMap hardwareMap;
    protected DcMotor leftFrontMotor;
    protected DcMotor rightFrontMotor;
    protected DcMotor leftRearMotor;
    protected DcMotor rightRearMotor;

    protected VuforiaLocalizer vuforiaLocalizer;

    public BaseRobot(HardwareMap hardwareMap) {
        this.hardwareMap = hardwareMap;
        this.leftFrontMotor = hardwareMap.get(DcMotor.class, "motorLF");
        this.rightFrontMotor = hardwareMap.get(DcMotor.class, "motorRF");
        this.leftRearMotor = hardwareMap.get(DcMotor.class, "motorLR");
        this.rightRearMotor = hardwareMap.get(DcMotor.class, "motorRR");
    }

    private void setChassisMotorMode(DcMotor.RunMode mode) {
        leftFrontMotor.setMode(mode);
        rightFrontMotor.setMode(mode);
        leftRearMotor.setMode(mode);
        rightRearMotor.setMode(mode);
    }

    private void incrementChassisMotorTargetPositions(int targetPosition) {
        incrementChassisMotorTargetPositions(
            targetPosition, targetPosition, targetPosition, targetPosition);
    }

    private void incrementChassisMotorTargetPositions(
            int leftFront, int rightFront, int leftRear, int rightRear) {
        leftFrontMotor.setTargetPosition(leftFrontMotor.getCurrentPosition() + leftFront);
        rightFrontMotor.setTargetPosition(rightFrontMotor.getCurrentPosition() + rightFront);
        leftRearMotor.setTargetPosition(leftRearMotor.getCurrentPosition() + leftRear);
        rightRearMotor.setTargetPosition(rightRearMotor.getCurrentPosition() + rightRear);
    }

    private void waitForBusyChassis() {
        while (leftFrontMotor.isBusy()
               || rightFrontMotor.isBusy()
               || leftRearMotor.isBusy()
               || rightRearMotor.isBusy()) { }
    }

    private void driveToTargetPosition(
            double power,
            int targetPosition,
            DcMotorSimple.Direction leftFrontDirection,
            DcMotorSimple.Direction rightFrontDirection,
            DcMotorSimple.Direction leftRearDirection,
            DcMotorSimple.Direction rightRearDirection) {
        incrementChassisMotorTargetPositions(targetPosition);
        setChassisDirection(
            leftFrontDirection, rightFrontDirection, leftRearDirection, rightRearDirection);
        setChassisMotorMode(DcMotor.RunMode.RUN_TO_POSITION);
        setChassisPower(power);
        waitForBusyChassis();
        stopChassis();
        setChassisMotorMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

    public void reset() {
        setChassisMotorMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        setChassisMotorMode(DcMotor.RunMode.RUN_USING_ENCODER);
        setChassisPower(0.0);
    }

    public final void setChassisDirection(DcMotorSimple.Direction direction) {
        setChassisDirection(direction, direction, direction, direction);
    }

    public final void setChassisDirection(
            DcMotorSimple.Direction leftFront,
            DcMotorSimple.Direction rightFront,
            DcMotorSimple.Direction leftRear,
            DcMotorSimple.Direction rightRear) {
        leftFrontMotor.setDirection(leftFront);
        rightFrontMotor.setDirection(rightFront.inverted());
        leftRearMotor.setDirection(leftRear);
        rightRearMotor.setDirection(rightRear.inverted());
    }

    public final void setChassisPower(double power) {
        setChassisPower(power, power, power, power);
    }

    public final void setChassisPower(
            double leftFront, double rightFront, double leftRear, double rightRear) {
        leftFrontMotor.setPower(leftFront);
        rightFrontMotor.setPower(rightFront);
        leftRearMotor.setPower(leftRear);
        rightRearMotor.setPower(rightRear);
    }

    public final void stopChassis() {
        setChassisPower(0.0);
    }

    public final void driveWithMecanumWheels(double power, double direction, double rotationAngle) {
        setChassisDirection(DcMotorSimple.Direction.FORWARD);
        setChassisPower(
            power * Math.cos(direction) + rotationAngle,
            power * Math.sin(direction) - rotationAngle,
            power * Math.sin(direction) - rotationAngle,
            power * Math.cos(direction) + rotationAngle
        );
    }

    public final void driveN(double power) {
        driveWithMecanumWheels(power, 0.7853981633974483, 0.0);
    }

    public final void driveNE(double power) {
        driveWithMecanumWheels(power, 1.5707963267948966, 0.0);
    }

    public final void driveE(double power) {
        driveWithMecanumWheels(power, 2.356194490192345, 0.0);
    }

    public final void driveSE(double power) {
        driveWithMecanumWheels(power, -3.141592653589793, 0.0);
    }

    public final void driveS(double power) {
        driveWithMecanumWheels(power, -2.356194490192345, 0.0);
    }

    public final void driveSW(double power) {
        driveWithMecanumWheels(power, -1.5707963267948966, 0.0);
    }

    public final void driveW(double power) {
        driveWithMecanumWheels(power, -0.7853981633974483, 0.0);
    }

    public final void driveNW(double power) {
        driveWithMecanumWheels(power, 0.0, 0.0);
    }

    public final void driveForward(double power, int targetPosition) {
        driveToTargetPosition(
                power,
                targetPosition,
                DcMotorSimple.Direction.FORWARD,
                DcMotorSimple.Direction.FORWARD,
                DcMotorSimple.Direction.FORWARD,
                DcMotorSimple.Direction.FORWARD
        );
    }

    public final void driveBackward(double power, int targetPosition) {
        driveToTargetPosition(
                power,
                targetPosition,
                DcMotorSimple.Direction.REVERSE,
                DcMotorSimple.Direction.REVERSE,
                DcMotorSimple.Direction.REVERSE,
                DcMotorSimple.Direction.REVERSE
        );
    }

    public final void driveLeftSideWay(double power, int targetPosition) {
        driveToTargetPosition(
                power,
                targetPosition,
                DcMotorSimple.Direction.REVERSE,
                DcMotorSimple.Direction.FORWARD,
                DcMotorSimple.Direction.FORWARD,
                DcMotorSimple.Direction.REVERSE
        );
    }

    public final void driveRightSideWay(double power, int targetPosition) {
        driveToTargetPosition(
                power,
                targetPosition,
                DcMotorSimple.Direction.FORWARD,
                DcMotorSimple.Direction.REVERSE,
                DcMotorSimple.Direction.REVERSE,
                DcMotorSimple.Direction.FORWARD
        );
    }

    public final void turnRoundClockwise(double power, int targetPosition) {
        driveToTargetPosition(
                power,
                targetPosition,
                DcMotorSimple.Direction.FORWARD,
                DcMotorSimple.Direction.REVERSE,
                DcMotorSimple.Direction.FORWARD,
                DcMotorSimple.Direction.REVERSE
        );
    }

    public final void turnRoundAnticlockwise(double power, int targetPosition) {
        driveToTargetPosition(
                power,
                targetPosition,
                DcMotorSimple.Direction.REVERSE,
                DcMotorSimple.Direction.FORWARD,
                DcMotorSimple.Direction.REVERSE,
                DcMotorSimple.Direction.FORWARD
        );
    }

    public final void createVuforiaLocalizer(VuforiaLocalizer.CameraDirection cameraDirection) {
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();
        parameters.vuforiaLicenseKey = VUFORIA_LICENSE_KEY;
        parameters.cameraDirection = cameraDirection;
        vuforiaLocalizer = ClassFactory.getInstance().createVuforia(parameters);
    }
}
