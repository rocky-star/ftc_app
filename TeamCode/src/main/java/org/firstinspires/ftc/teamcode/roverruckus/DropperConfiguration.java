package org.firstinspires.ftc.teamcode.roverruckus;

import com.acmerobotics.dashboard.config.Config;

@Config
public class DropperConfiguration {
    public static double DROPPER_RAISING_POSITION = 0.5;
    public static double DROPPER_LOWERING_POSITION = 0.89;
}
