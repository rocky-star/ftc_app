package org.firstinspires.ftc.teamcode.roverruckus;

import com.qualcomm.robotcore.hardware.HardwareMap;

public class LegacyRoverRuckusRobot extends RoverRuckusRobot {
    private boolean isDropperRaised = false;
    private boolean isCollectorRaised = false;

    public LegacyRoverRuckusRobot(HardwareMap hardwareMap) {
        super(hardwareMap);
    }

    public final void resetDropper() { }

    public final void resetCollector() { }

    public final void switchDropper() {
        if (isDropperRaised) {
            lowerDropper();
        } else {
            raiseDropper();
        }
        isDropperRaised = !isDropperRaised;
    }

    public final void switchCollector() {
        if (isCollectorRaised) {
            lowerCollector();
        } else {
            raiseCollector();
        }
        isCollectorRaised = !isCollectorRaised;
    }

    public final void switchCollectorRunning() { }
}
