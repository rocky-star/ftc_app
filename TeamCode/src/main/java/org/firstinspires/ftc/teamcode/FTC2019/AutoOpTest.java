package org.firstinspires.ftc.teamcode.FTC2019;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

/**
 * Created by my on 2018/2/22.
 */
@TeleOp(name = "AutoOpTest")
public class AutoOpTest extends LinearOpMode {

    static final double FF_UP = 1;
    static final double FF_DOWN = 0.24;
    static final double FF_MIDDLE = 0.6;
    static final double BF_UP = 0.5;
    static final double BF_DOWN = 0.89;
    static final double BF_UPUP = 0.1;

    DcMotor theMotor;


    @Override
    public void runOpMode() throws InterruptedException {
        theMotor = hardwareMap.dcMotor.get("m");
        theMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        theMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        waitForStart();
//
        driveForwardDistance(1, 0.4);
        while (opModeIsActive()) {
            idle();
        }

        //theMotor.setPower(0.4);
        //sleep(5000);
        //theMotor.setPower(0.0);

        //FrontUP();

    }

    void driveForwardDistance(double forwardInches, double leftFrontPower) {
//         设置4个电机转动的目标
        int newLeftFrontTarget;
        int newRightFrontTarget;
        int newRightBackTarget;
        int newLeftBackTarget;


        if (opModeIsActive()) {

            newLeftFrontTarget = theMotor.getCurrentPosition() + (int) (forwardInches * 1680.0);
            //newRightFrontTarget = bot.rightFront.getCurrentPosition() + (int) (forwardInches * bot.COUNTS_PER_INCH);
            //newRightBackTarget = bot.rightBack.getCurrentPosition() + (int) (forwardInches * bot.COUNTS_PER_INCH);
            //newLeftBackTarget = bot.leftBack.getCurrentPosition() + (int) (forwardInches * bot.COUNTS_PER_INCH);

//            让车行驶至目标
            theMotor.setTargetPosition(newLeftFrontTarget);
            //bot.rightFront.setTargetPosition(-newRightFrontTarget);
            //bot.rightBack.setTargetPosition(-newRightBackTarget);
            //bot.leftBack.setTargetPosition(newLeftBackTarget);

//             设置行驶模式
            theMotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
//             设置行驶速度
            theMotor.setPower(leftFrontPower);

            while (opModeIsActive() && theMotor.isBusy()) {
                telemetry.addData("LF", theMotor.getCurrentPosition());
                telemetry.update();
            }

//        设置电机停止模式
            theMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
//          停止移动
            theMotor.setPower(0.0);
//          设置编码器
            theMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        }
    }

}
