package org.firstinspires.ftc.teamcode.roverruckus;

import com.acmerobotics.dashboard.config.Config;

@Config
public class AutonomousConfiguration {
    public static double AS1_FORWARD_ROUNDS = 1.0;
    public static double AS2_SIDE_WAY_ROUNDS = 1.0;
    public static double AS3_FORWARD_ROUNDS = 1.0;
    public static double AS4_TURN_ROUND_ROUNDS = 1.0;
    public static double AS5_BACKWARD_ROUNDS = 1.0;

    public static double BS1_FORWARD_ROUNDS = 1.0;
    public static double BS2_SIDE_WAY_ROUNDS = 1.0;
    public static double BS3_FORWARD_ROUNDS = 1.0;

    public static double AUTONOMOUS_CHASSIS_POWER = 0.1;
    public static double COUNTS_PER_ROUNDS = 1680.0;
}
