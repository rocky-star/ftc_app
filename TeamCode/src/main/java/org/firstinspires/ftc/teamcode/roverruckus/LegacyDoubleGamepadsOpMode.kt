package org.firstinspires.ftc.teamcode.roverruckus

import com.acmerobotics.dashboard.FtcDashboard
import com.acmerobotics.dashboard.telemetry.TelemetryPacket
import com.qualcomm.robotcore.eventloop.opmode.Disabled
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode
import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import kotlin.math.abs
import kotlin.math.hypot

import org.firstinspires.ftc.teamcode.roverruckus.InputConfiguration.JOYSTICK_THRESHOLD;
import org.firstinspires.ftc.teamcode.roverruckus.InputConfiguration.KEY_PRESSING_DELAY

@Disabled
@TeleOp(name = "LEGACY: Double Gamepads", group = "Legacy")
class LegacyDoubleGamepadsOpMode : LinearOpMode() {
    override fun runOpMode() {
        val robot = LegacyRoverRuckusRobot(hardwareMap)
        val dashboard = FtcDashboard.getInstance()
        var packet: TelemetryPacket

        waitForStart()
        robot.reset()
        robot.resetDropper()
        robot.resetCollector()

        while (opModeIsActive()) {
            packet = TelemetryPacket()
            val power = hypot(gamepad1.left_stick_x, gamepad1.left_stick_y).toDouble()
            packet.put("Power", power)

            if (gamepad1.left_stick_x > JOYSTICK_THRESHOLD
                    && gamepad1.left_stick_y < -JOYSTICK_THRESHOLD) {
                packet.put("Direction", "NE")
                robot.driveNE(power)
            } else if (gamepad1.left_stick_x > JOYSTICK_THRESHOLD
                    && gamepad1.left_stick_y > JOYSTICK_THRESHOLD) {
                packet.put("Direction", "SE")
                robot.driveSE(power)
            } else if (gamepad1.left_stick_x < -JOYSTICK_THRESHOLD
                    && gamepad1.left_stick_y > JOYSTICK_THRESHOLD) {
                packet.put("Direction", "SW")
                robot.driveSW(power)
            } else if (gamepad1.left_stick_x < -JOYSTICK_THRESHOLD
                    && gamepad1.left_stick_y < -JOYSTICK_THRESHOLD) {
                packet.put("Direction", "NW")
                robot.driveNW(power)
            } else if (gamepad1.left_stick_x > JOYSTICK_THRESHOLD) {
                packet.put("Direction", "E")
                robot.driveE(power)
            } else if (gamepad1.left_stick_x < -JOYSTICK_THRESHOLD) {
                packet.put("Direction", "W")
                robot.driveW(power)
            } else if (gamepad1.left_stick_y > JOYSTICK_THRESHOLD) {
                packet.put("Direction", "S")
                robot.driveS(power)
            } else if (gamepad1.left_stick_y < -JOYSTICK_THRESHOLD) {
                packet.put("Direction", "N")
                robot.driveN(power)
            } else {
                packet.put("Direction", "Stop")
                robot.stopChassis()
            }

            packet.put("Rotation", gamepad1.right_stick_x)
            if (abs(gamepad1.right_stick_x) > JOYSTICK_THRESHOLD) {
                robot.driveWithMecanumWheels(
                        abs(gamepad1.right_stick_x).toDouble(),
                        0.0,
                        gamepad1.right_stick_x.toDouble()
                )
            }

            if (gamepad2.left_bumper) {
                sleep()
                robot.switchDropper()
            }
            if (gamepad2.right_bumper) {
                sleep()
                robot.switchCollector()
            }
            if (gamepad2.a) {
                sleep()
                robot.switchCollectorRunning()
            }

            robot.setDropperPower(-gamepad2.left_stick_y.toDouble())
            robot.setCollectorPower(-gamepad2.right_stick_y.toDouble())

            dashboard.sendTelemetryPacket(packet)
        }
    }

    fun sleep() {
        try {
            Thread.sleep(KEY_PRESSING_DELAY.toLong())
        } catch (e: InterruptedException) { }
    }
}
