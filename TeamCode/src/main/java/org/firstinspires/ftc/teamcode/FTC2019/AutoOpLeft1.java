package org.firstinspires.ftc.teamcode.FTC2019;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

/**
 * Created by my on 2018/2/22.
 */
@TeleOp(name = "AutoOpLeft1")
public class AutoOpLeft1 extends AutoOpBase {
    @Override
    public void runOpMode() throws InterruptedException {
        initBot();
        waitForStart();

        driveLeftDistance(5, 0.1, 0.1, 0.9, 0.9);
        while (opModeIsActive()) {
            idle();
        }
    }
}
