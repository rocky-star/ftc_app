package org.firstinspires.ftc.teamcode.roverruckus;

import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.teamcode.BaseRobot;

import static org.firstinspires.ftc.teamcode.roverruckus.CollectorConfiguration.COLLECTOR_LOWERING_POSITION;
import static org.firstinspires.ftc.teamcode.roverruckus.CollectorConfiguration.COLLECTOR_RAISING_POSITION;
import static org.firstinspires.ftc.teamcode.roverruckus.CollectorConfiguration.MINERAL_COLLECTION_POWER;
import static org.firstinspires.ftc.teamcode.roverruckus.DropperConfiguration.DROPPER_LOWERING_POSITION;
import static org.firstinspires.ftc.teamcode.roverruckus.DropperConfiguration.DROPPER_RAISING_POSITION;
import static org.firstinspires.ftc.teamcode.roverruckus.RobotConnectionConfiguration.ROBOT_CONNECTION_DURATION_POWER;
import static org.firstinspires.ftc.teamcode.roverruckus.RobotConnectionConfiguration.ROBOT_CONNECTION_DURATION_TIME;

public class RoverRuckusRobot extends BaseRobot {
    private static final String TF_MODEL_ASSET_NAME = "RoverRuckus.tflite";
    private static final String TF_GOLD_MINERAL_LABEL = "Gold Mineral";
    private static final String TF_SILVER_MINERAL_LABEL = "Silver Mineral";

    private Servo dropperServo;
    private DcMotor dropperMotor;

    private Servo collectorServo;
    private DcMotor collectorMotor;

    private CRServo mineralCollectionServo;
    private DcMotor robotConnectionMotor;
    private Servo phoneMovingServo;

    private TFObjectDetector tfObjectDetector;

    public RoverRuckusRobot(HardwareMap hardwareMap) {
        super(hardwareMap);

        dropperServo = hardwareMap.get(Servo.class, "dropper");
        dropperMotor = hardwareMap.get(DcMotor.class, "dropperMotor");
        dropperMotor.setDirection(DcMotorSimple.Direction.REVERSE);
        dropperMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        collectorServo = hardwareMap.get(Servo.class, "collector");
        collectorMotor = hardwareMap.get(DcMotor.class, "collectorMotor");
        collectorMotor.setDirection(DcMotorSimple.Direction.REVERSE);
        collectorMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        mineralCollectionServo = hardwareMap.get(CRServo.class, "collectorCR");
        robotConnectionMotor = hardwareMap.get(DcMotor.class, "robotConnecting");
        phoneMovingServo = hardwareMap.get(Servo.class, "phone");
    }

    @Override
    public final void reset() {
        super.reset();

        stopCollector();
        raiseCollector();

        stopDropper();
        lowerDropper();

        stopMineralCollection();
        stopRobotConnection();
    }

    public final void raiseDropper() {
        dropperServo.setPosition(DROPPER_RAISING_POSITION);
    }

    public final void lowerDropper() {
        dropperServo.setPosition(DROPPER_LOWERING_POSITION);
    }

    public final void setDropperPower(double power) {
        dropperMotor.setPower(power);
    }

    public final void stopDropper() {
        dropperMotor.setPower(0.0);
    }

    public final void raiseCollector() {
        collectorServo.setPosition(COLLECTOR_RAISING_POSITION);
    }

    public final void lowerCollector() {
        collectorServo.setPosition(COLLECTOR_LOWERING_POSITION);
    }

    public final void setCollectorPower(double power) {
        collectorMotor.setPower(power);
    }

    public final void stopCollector() {
        collectorMotor.setPower(0.0);
    }

    public final void startMineralCollection() {
        mineralCollectionServo.setPower(MINERAL_COLLECTION_POWER);
    }

    public final void stopMineralCollection() {
        mineralCollectionServo.setPower(0.0);
    }

    public final void setRobotConnectionDirection(DcMotorSimple.Direction direction) {
        robotConnectionMotor.setDirection(direction.inverted());
    }

    public final void setRobotConnectionPower(double power) {
        robotConnectionMotor.setPower(power);
    }

    public final void stopRobotConnection() {
        robotConnectionMotor.setPower(0.0);
    }

    public final void startRobotConnectionWithDuration() {
        setRobotConnectionDirection(DcMotorSimple.Direction.FORWARD);
        robotConnectionMotor.setPower(ROBOT_CONNECTION_DURATION_POWER);
        try {
            Thread.sleep(ROBOT_CONNECTION_DURATION_TIME);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        stopRobotConnection();
    }

    public final void movePhone(double position) {
        phoneMovingServo.setPosition(position);
    }

    public final void createTFObjectDetector() {
        int monitorViewID = hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters parameters = new TFObjectDetector.Parameters(monitorViewID);
        tfObjectDetector = ClassFactory.getInstance().createTFObjectDetector(
                parameters, vuforiaLocalizer);
    }

    public final void loadTFModel() {
        tfObjectDetector.loadModelFromAsset(
                TF_MODEL_ASSET_NAME, TF_GOLD_MINERAL_LABEL, TF_SILVER_MINERAL_LABEL);
    }
}
