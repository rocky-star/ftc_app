package org.firstinspires.ftc.teamcode.FTC2019;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;

import static java.lang.Thread.sleep;

/**
 * Created by my on 2018/2/22.
 */

public abstract class AutoOpBase extends LinearOpMode {

    Robot bot = new Robot();

    //        把设备全部初始化
    void initBot() throws InterruptedException {
//所有需要的硬件数据都在下一行的init（Ctrl）里面
        bot.init(hardwareMap, telemetry);
//        编码器
        bot.resetEncoders();
//        使用编码器让车运动
        bot.setUseEncoderMode();
    }


    /**
     * 直行行驶至目标
     *
     * @param forwardInches 目标距离
     *                      //     * @param driveSpeed    速度
     */

    void driveForwardDistance(double forwardInches, double leftFrontPower, double rightFrontPower, double rightBackPower, double leftBackPower) {
//         设置4个电机转动的目标
        int newLeftFrontTarget;
        int newRightFrontTarget;
        int newRightBackTarget;
        int newLeftBackTarget;


        if (opModeIsActive()) {

            newLeftFrontTarget = bot.leftFront.getCurrentPosition() + (int) (forwardInches * bot.COUNTS_PER_INCH);
            newRightFrontTarget = bot.rightFront.getCurrentPosition() + (int) (forwardInches * bot.COUNTS_PER_INCH);
            newRightBackTarget = bot.rightBack.getCurrentPosition() + (int) (forwardInches * bot.COUNTS_PER_INCH);
            newLeftBackTarget = bot.leftBack.getCurrentPosition() + (int) (forwardInches * bot.COUNTS_PER_INCH);

//            让车行驶至目标
            bot.leftFront.setTargetPosition(newLeftFrontTarget);
            bot.rightFront.setTargetPosition(-newRightFrontTarget);
            bot.rightBack.setTargetPosition(-newRightBackTarget);
            bot.leftBack.setTargetPosition(newLeftBackTarget);

//             设置行驶模式
            bot.setRunToPositionMode();
//             设置行驶速度
            bot.driveForward(leftFrontPower, rightFrontPower, rightBackPower, leftBackPower);

            while (opModeIsActive() && bot.isBusy()) {
                bot.telemetry.addData("LF", bot.leftFront.getCurrentPosition());
                bot.telemetry.addData("RF", bot.rightFront.getCurrentPosition());
                bot.telemetry.addData("RB", bot.rightBack.getCurrentPosition());
                bot.telemetry.addData("LB", bot.leftBack.getCurrentPosition());
                bot.telemetry.update();
            }

//        设置电机停止模式
            bot.setMotorStopMode();
//          停止移动
            bot.stopDriving();
//          设置编码器
            bot.setUseEncoderMode();
        }
    }

    void driveForwardByTime(
            long duration,
            double leftFrontPower,
            double rightFrontPower,
            double leftRearPower,
            double rightRearPower) {
        if (opModeIsActive()) {
            bot.leftFront.setPower(leftFrontPower);
            bot.rightFront.setPower(-rightFrontPower);
            bot.leftBack.setPower(leftRearPower);
            bot.rightBack.setPower(-rightRearPower);
            sleep(duration);
            bot.stopDriving();
        }
    }


    /**
     * 反向行驶至目标距离
     *
     * @param backwardInches 目标距离
     */


    void driveBackwardDistance(double backwardInches, double leftFrontPower, double rightFrontPower, double rightBackPower, double leftBackPower) {
//         设置4个电机转动的目标
        int newLeftFrontTarget;
        int newRightFrontTarget;
        int newRightBackTarget;
        int newLeftBackTarget;

        if (opModeIsActive()) {

            newLeftFrontTarget = bot.leftFront.getCurrentPosition() + (int) (backwardInches * bot.COUNTS_PER_INCH);
            newRightFrontTarget = bot.rightFront.getCurrentPosition() + (int) (backwardInches * bot.COUNTS_PER_INCH);
            newRightBackTarget = bot.rightBack.getCurrentPosition() + (int) (backwardInches * bot.COUNTS_PER_INCH);
            newLeftBackTarget = bot.leftBack.getCurrentPosition() + (int) (backwardInches * bot.COUNTS_PER_INCH);

//            让车行驶至目标
            bot.leftFront.setTargetPosition(-newLeftFrontTarget);
            bot.rightFront.setTargetPosition(newRightFrontTarget);
            bot.rightBack.setTargetPosition(newRightBackTarget);
            bot.leftBack.setTargetPosition(-newLeftBackTarget);

//             设置行驶模式
            bot.setRunToPositionMode();
//             设置行驶速度
            bot.driveBackward(leftFrontPower, rightFrontPower, rightBackPower, leftBackPower);

            while (opModeIsActive() && bot.isBusy()) {
                bot.telemetry.addData("LF", bot.leftFront.getCurrentPosition());
                bot.telemetry.addData("RF", bot.rightFront.getCurrentPosition());
                bot.telemetry.addData("RB", bot.rightBack.getCurrentPosition());
                bot.telemetry.addData("LB", bot.leftBack.getCurrentPosition());
                bot.telemetry.update();
            }

//        设置电机停止模式
//            bot.setMotorStopMode();
//          停止移动
            bot.stopDriving();

            bot.resetEncoders();
//          设置编码器
            bot.setUseEncoderMode();
        }
    }

    void driveBackwardByTime(
            long duration,
            double leftFrontPower,
            double rightFrontPower,
            double leftRearPower,
            double rightRearPower) {
        if (opModeIsActive()) {
            bot.leftFront.setPower(-leftFrontPower);
            bot.rightFront.setPower(rightFrontPower);
            bot.leftBack.setPower(-leftRearPower);
            bot.rightBack.setPower(rightRearPower);
            sleep(duration);
            bot.stopDriving();
        }
    }

//    /**
//     * 左平移至目标
//     *
//     * @param leftInches 目标距离
//     * @param driveSpeed 行驶速度
//     */

    void driveLeftDistance(double leftInches, double leftFrontpower, double rightFrontPower, double rightBackPower, double leftBackPower) {

//         设置4个电机转动的目标
        int newLeftFrontTarget;
        int newRightFrontTarget;
        int newRightBackTarget;
        int newLeftBackTarget;


        if (opModeIsActive()) {

            newLeftFrontTarget = bot.leftFront.getCurrentPosition() + (int) (leftInches * bot.COUNTS_PER_INCH);
            newRightFrontTarget = bot.rightFront.getCurrentPosition() + (int) (leftInches * bot.COUNTS_PER_INCH);
            newRightBackTarget = bot.rightBack.getCurrentPosition() + (int) (leftInches * bot.COUNTS_PER_INCH);
            newLeftBackTarget = bot.leftBack.getCurrentPosition() + (int) (leftInches * bot.COUNTS_PER_INCH);

//            让车行驶至目标
            bot.leftFront.setTargetPosition(-newLeftFrontTarget);
            bot.rightFront.setTargetPosition(-newRightFrontTarget);
            bot.rightBack.setTargetPosition(newRightBackTarget);
            bot.leftBack.setTargetPosition(newLeftBackTarget);

//             设置行驶模式
            bot.setRunToPositionMode();
//             设置行驶速度
            bot.driveLeft(leftFrontpower, rightFrontPower, rightBackPower, leftBackPower);

            while (opModeIsActive() && bot.isBusy()) {

            }

//        设置电机停止模式
//            bot.setMotorStopMode();
//          停止移动
            bot.stopDriving();
//          设置编码器
            bot.setUseEncoderMode();
        }
    }

//    /**
//     * 右平移至目标
//     *
//     * @param forwardInches 目标距离
//     * @param driveSpeed    行驶速度
//     */

    void driveRightDistance(double forwardInches, double leftFrontPower, double rightFrontPower, double rightBackPower, double leftBackPower) {

//         设置4个电机转动的目标
        int newLeftFrontTarget;
        int newRightFrontTarget;
        int newRightBackTarget;
        int newLeftBackTarget;


        if (opModeIsActive()) {

            newLeftFrontTarget = bot.leftFront.getCurrentPosition() + (int) (forwardInches * bot.COUNTS_PER_INCH);
            newRightFrontTarget = bot.rightFront.getCurrentPosition() + (int) (forwardInches * bot.COUNTS_PER_INCH);
            newRightBackTarget = bot.rightBack.getCurrentPosition() + (int) (forwardInches * bot.COUNTS_PER_INCH);
            newLeftBackTarget = bot.leftBack.getCurrentPosition() + (int) (forwardInches * bot.COUNTS_PER_INCH);

//            让车行驶至目标
            bot.leftFront.setTargetPosition(newLeftFrontTarget);
            bot.rightFront.setTargetPosition(newRightFrontTarget);
            bot.rightBack.setTargetPosition(-newRightBackTarget);
            bot.leftBack.setTargetPosition(-newLeftBackTarget);

//             设置行驶模式
            bot.setRunToPositionMode();
//             设置行驶速度
            bot.driveRight(leftFrontPower, rightFrontPower, rightBackPower, leftBackPower);

            while (opModeIsActive() && bot.isBusy()) {

            }

//        设置电机停止模式
//            bot.setMotorStopMode();
//          停止移动
            bot.stopDriving();
//          设置编码器
            bot.setUseEncoderMode();
        }
    }

    void turnLeftDistance(double forwardInches, double leftFrontPower, double rightFrontPower, double rightBackPower, double leftBackPower) {
//         设置4个电机转动的目标
        int newLeftFrontTarget;
        int newRightFrontTarget;
        int newRightBackTarget;
        int newLeftBackTarget;


        if (opModeIsActive()) {

            newLeftFrontTarget = bot.leftFront.getCurrentPosition() - (int) (forwardInches * bot.COUNTS_PER_INCH);
            newRightFrontTarget = bot.rightFront.getCurrentPosition() - (int) (forwardInches * bot.COUNTS_PER_INCH);
            newRightBackTarget = bot.rightBack.getCurrentPosition() - (int) (forwardInches * bot.COUNTS_PER_INCH);
            newLeftBackTarget = bot.leftBack.getCurrentPosition() - (int) (forwardInches * bot.COUNTS_PER_INCH);

//            让车行驶至目标
            bot.leftFront.setTargetPosition(newLeftFrontTarget);
            bot.rightFront.setTargetPosition(newRightFrontTarget);
            bot.rightBack.setTargetPosition(newRightBackTarget);
            bot.leftBack.setTargetPosition(newLeftBackTarget);

//             设置行驶模式
            bot.setRunToPositionMode();
//             设置行驶速度
            bot.driveForward(leftFrontPower, rightFrontPower, rightBackPower, leftBackPower);

            while (opModeIsActive() && bot.isBusy()) {

            }

//        设置电机停止模式
            bot.setMotorStopMode();
//          停止移动
            bot.stopDriving();
//          设置编码器
            bot.setUseEncoderMode();
        }
    }

    void turnRightDistance(double forwardInches, double leftFrontPower, double rightFrontPower, double rightBackPower, double leftBackPower) {
//         设置4个电机转动的目标
        int newLeftFrontTarget;
        int newRightFrontTarget;
        int newRightBackTarget;
        int newLeftBackTarget;


        if (opModeIsActive()) {

            newLeftFrontTarget = bot.leftFront.getCurrentPosition() + (int) (forwardInches * bot.COUNTS_PER_INCH);
            newRightFrontTarget = bot.rightFront.getCurrentPosition() + (int) (forwardInches * bot.COUNTS_PER_INCH);
            newRightBackTarget = bot.rightBack.getCurrentPosition() + (int) (forwardInches * bot.COUNTS_PER_INCH);
            newLeftBackTarget = bot.leftBack.getCurrentPosition() + (int) (forwardInches * bot.COUNTS_PER_INCH);

//            让车行驶至目标
            bot.leftFront.setTargetPosition(newLeftFrontTarget);
            bot.rightFront.setTargetPosition(newRightFrontTarget);
            bot.rightBack.setTargetPosition(newRightBackTarget);
            bot.leftBack.setTargetPosition(newLeftBackTarget);

//             设置行驶模式
            bot.setRunToPositionMode();
//             设置行驶速度
            bot.driveForward(leftFrontPower, rightFrontPower, rightBackPower, leftBackPower);

            while (opModeIsActive() && bot.isBusy()) {

            }

//        设置电机停止模式
            bot.setMotorStopMode();
//          停止移动
            bot.stopDriving();
//          设置编码器
            bot.setUseEncoderMode();
        }
    }
}
