package org.firstinspires.ftc.teamcode.FTC2019;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import static org.firstinspires.ftc.teamcode.FTC2019.AutoOpTest.BF_DOWN;
import static org.firstinspires.ftc.teamcode.FTC2019.AutoOpTest.BF_UPUP;

@Autonomous(name = "辅位红")
public class FuweiHongOpMode extends AutoOpBase {
    private static final double LF_POWER = 0.4;
    private static final double RF_POWER = 0.6;
    private static final double LB_POWER = 0.6;
    private static final double RB_POWER = 0.6;

    @Override
    public void runOpMode() throws InterruptedException {
        initBot();
        bot.g.setPower(1.0);
        waitForStart();
        if (opModeIsActive()) {
            bot.g.setPower(-1.0);
            sleep(1000);
            driveRightDistance(0.5, LF_POWER, RF_POWER, LB_POWER, RB_POWER);
            bot.g.setPower(0.0);

            bot.bf.setPosition(AutoOpTest.BF_DOWN);
            bot.ff.setPosition(AutoOpTest.FF_UP);
            sleep(500);

            //bot.g.setPower(0.5);
            //sleep(3000);
            //driveRightDistance(10, LF_POWER, RF_POWER, RB_POWER, LB_POWER);
            //bot.g.setPower(-0.5);
            //sleep(3000);
            //bot.g.setPower(0.0);

            telemetry.addLine("1. Forward");
            telemetry.update();
            driveForwardDistance(3.75, LF_POWER, RF_POWER, RB_POWER, LB_POWER);

            bot.g.setPower(1.0);
            sleep(2000);
            bot.g.setPower(0.0);

            telemetry.addLine("2. Turn Left");
            telemetry.update();
            turnLeftDistance(6, LF_POWER, RF_POWER, RB_POWER, LB_POWER);

            telemetry.addLine("3. Backward");
            telemetry.update();
            bot.resetEncoders();
            bot.setUseEncoderMode();
            driveForwardDistance(1.75, LF_POWER, RF_POWER, RB_POWER, LB_POWER);

            bot.y.setPower(-1.0);
            sleep(2000);
            bot.bf.setPosition(BF_UPUP);
            sleep(1000);
            bot.bf.setPosition(BF_DOWN);
            sleep(500);
            bot.y.setPower(1.0);
            sleep(5000);
            bot.y.setPower(0.0);

            driveBackwardDistance(2, LB_POWER, RB_POWER, RF_POWER, LF_POWER);

            bot.x.setPower(-1.0);
            sleep(250);
            bot.x.setPower(0.0);
            driveRightDistance(10, 0.8, 0.8, 0.4, 0.4);
            bot.x.setPower(1.0);
            sleep(2000);
            bot.x.setPower(0.0);
            bot.resetEncoders();
            bot.setUseEncoderMode();
            driveForwardDistance(6.5, LF_POWER, RF_POWER, RB_POWER, LB_POWER);
        }
        while (opModeIsActive()) {
            idle();
        }
    }
}
