package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;

@Disabled
@TeleOp(name = "CRServo Demonstration")
public class CRServoOpMode extends LinearOpMode {
    @Override
    public void runOpMode() throws InterruptedException {
        CRServo crServo = hardwareMap.get(CRServo.class, "crServo");

        waitForStart();
        for(;;) {
            crServo.setPower(-gamepad1.left_stick_y);
            telemetry.addData("Power", crServo.getPower());
            telemetry.update();
        }
    }
}
